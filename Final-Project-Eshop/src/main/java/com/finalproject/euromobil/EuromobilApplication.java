package com.finalproject.euromobil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuromobilApplication {

	public static void main(String[] args) {
		SpringApplication.run(EuromobilApplication.class, args);
	}
        
}