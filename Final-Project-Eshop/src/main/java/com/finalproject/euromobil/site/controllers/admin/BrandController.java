package com.finalproject.euromobil.site.controllers.admin;

import com.finalproject.euromobil.site.entities.Brand;
import com.finalproject.euromobil.site.forms.BrandForm;
import com.finalproject.euromobil.site.services.BrandService;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/brands")
public class BrandController 
{
    @Inject
    private BrandService brandService;
    
    @RequestMapping({"/", ""})
    public String brands(Map<String, Object> model)
    {
        model.put("brands", brandService.findAll());
        return "/brands/brands";
    }
    
    @GetMapping("/add-brands")
    public String addBrandForm(Model model)
    {
        model.addAttribute("brandForm", new BrandForm());
        return "/brands/add-brands";
    }
    
    @PostMapping("/add-brands")
    public String addBrand(@Valid BrandForm brandForm, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
        if(brandService.existsByName(brandForm.getName()))
        {
            bindingResult.addError(new FieldError("brandForm", "name", "Taková značka už existuje"));
        }
        
        if (bindingResult.hasErrors()) 
        {
            System.out.println(bindingResult.getFieldError());
            return "/brands/add-brands";
        }
        
        try
        {
        Brand b = new Brand();
        b.setName(brandForm.getName());        
        brandService.save(b);
        
        redirectAttributes.addFlashAttribute("successMessage", "Značka " + b.getName() + " byla uložena");
        
        System.out.println("Brand: " + b.getName() + " pridan.");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Značku se nepovedlo uložit");
        }
        
        return "redirect:/admin/brands";
    }   
    
    @GetMapping("/edit-brand/{brandId}")
    public String editBrandForm(@PathVariable long brandId, Model model, RedirectAttributes redirectAttributes)
    {
        if(!brandService.findById(brandId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Značka s id " + brandId + " nebyla nalezena");
            return "redirect:/admin/brands";
        }
        
        BrandForm brandForm = new BrandForm();
        Brand b = brandService.findById(brandId).get();
        
        brandForm.setName(b.getName());
        
        model.addAttribute("brandForm", brandForm);
        model.addAttribute("brandId", brandId);
        
        return "/brands/edit-brands";
    }
    
    @PostMapping("/edit-brand/{brandId}")
    public String editBrand(@Valid BrandForm brandForm, BindingResult bindingResult, @PathVariable long brandId, RedirectAttributes redirectAttributes)
    {       
        if (bindingResult.hasErrors()) 
        {
            return "/brands/edit-brands";
        }
        
        try
        {
        Brand b = brandService.findById(brandId).get();
        b.setName(brandForm.getName());
        brandService.save(b);
        
        redirectAttributes.addFlashAttribute("successMessage", "Značka s id " + brandId + " byla upravena");
        
        }
        catch(DataIntegrityViolationException ex)
        {
            bindingResult.addError(new FieldError("brandForm", "name", "Taková značka už existuje"));
            
            return "/brands/edit-brands";
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Značku s id " + brandId + " se nepodařilo upravit");
        }
        
        return "redirect:/admin/brands";
    }
    
    @GetMapping("/delete-brand/{brandId}")
    public String deleteBrand(@PathVariable long brandId, RedirectAttributes redirectAttributes)
    {
        if(!brandService.findById(brandId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Značka s id " + brandId + " nebyla nalezena");
            return "redirect:/admin/brands";
        }
        
        try
        {
            brandService.deleteById(brandId);
            
            redirectAttributes.addFlashAttribute("successMessage", "Značka s id " + brandId + " byla smazána");
            
        }
        catch(ConstraintViolationException | DataIntegrityViolationException ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Na značce s id " + brandId + " je závislé zboží - nelze ji smazat");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Značku s id " + brandId + " se nepodařilo smazat");
        }
        
        return "redirect:/admin/brands";
    }
}
