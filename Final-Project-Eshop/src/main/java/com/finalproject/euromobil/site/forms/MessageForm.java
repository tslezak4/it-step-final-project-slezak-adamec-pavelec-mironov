package com.finalproject.euromobil.site.forms;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class MessageForm 
{
    @Email
    String email;
    
    @Size(min = 5, max = 200)
    String message;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
