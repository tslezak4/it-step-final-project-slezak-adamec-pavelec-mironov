package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Status;
import com.finalproject.euromobil.site.repositories.StatusRepository;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class DefaultStatusService implements StatusService
{
    @Inject
    private StatusRepository statusRepository;
    
    @Override
    public Optional<Status> findById(Long id)
    {
        return statusRepository.findById(id);
    }
    
}
