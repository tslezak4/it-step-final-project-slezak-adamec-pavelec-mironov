package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.entities.OrderItem;
import java.util.List;

public interface OrderItemService 
{
    List<OrderItem> findAllForOrder(int orderId);
    
    List<OrderItem> createFromCart(Order o, long userId);
    
    void deleteAllByOrderId(Long orderId);
}
