package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Roles;
import org.springframework.data.repository.CrudRepository;

public interface RolesRepository extends CrudRepository<Roles, Long>
{
    @Override
    Roles save(Roles r);
}
