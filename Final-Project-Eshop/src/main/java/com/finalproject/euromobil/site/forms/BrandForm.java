
package com.finalproject.euromobil.site.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class BrandForm {
    
    @NotBlank
    @Size(min=2, max=60)
    private String name; 

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
}
