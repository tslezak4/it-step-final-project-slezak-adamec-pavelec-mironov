package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Cart;
import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.entities.OrderItem;
import com.finalproject.euromobil.site.repositories.CartRepository;
import com.finalproject.euromobil.site.repositories.OrderItemRepository;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DefaultOrderItemService implements OrderItemService
{
    @Inject
    private OrderItemRepository orderItemRepository;
    
    @Inject
    private CartRepository cartRepository;
    
    @Override
    public List<OrderItem> findAllForOrder(int orderId)
    {
        return orderItemRepository.findAllForOrder(orderId);
    }
    
    @Override
    @Transactional
    public void deleteAllByOrderId(Long orderId)
    {
        orderItemRepository.deleteAllByOrderId(orderId);
    }
    
    @Override
    @Transactional
    public List<OrderItem> createFromCart(Order o, long userId)
    { 
        List<OrderItem> items = new ArrayList<>();
        
        for(Cart cartItem : cartRepository.findByUserId(userId))
        {
            OrderItem oi = new OrderItem();
            
            oi.setGoods(cartItem.getGood());
            oi.setFixedPrice(cartItem.getGood().getPrice());
            oi.setGoodsQuantity(cartItem.getGoodsQuantity());
            oi.setOrder(o);
            
            orderItemRepository.save(oi);
            
            items.add(oi);
        }
        
        return items;
    }
}