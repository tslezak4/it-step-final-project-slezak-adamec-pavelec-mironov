package com.finalproject.euromobil.site.validation.annotations;

import com.finalproject.euromobil.site.validation.validators.PasswordConstraintEditValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = PasswordConstraintEditValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPasswordEdit 
{
     String message() default "Musí obsahovat aspoň 8 znaků, velké i malé písmeno a číslo";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

