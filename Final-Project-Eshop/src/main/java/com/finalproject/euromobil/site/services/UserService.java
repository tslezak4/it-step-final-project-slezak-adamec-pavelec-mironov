package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService 
{
    Page<User> findAll(Pageable pageable);
    
    Page<User> findFiltered(Pageable pageable, String name, String login, String authority);
    
    User save(User u);
    
    Optional<User> findById(long id);
    
    User findByLogin(String login);
    
    void deleteById(long id);
    
    boolean existsByLogin(String login);
}
