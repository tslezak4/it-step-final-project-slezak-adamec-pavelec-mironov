package com.finalproject.euromobil.site.forms;

import com.finalproject.euromobil.site.validation.annotations.EmptyListConstraint;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

public class GoodsForm 
{
    @NotBlank
    @Size(min=2, max=200)
    private String name;
    
    private Long brand;
    
    @NotBlank
    @Size(min=5, max=400)
    private String description;
    
    @Range(min=1, max=200000)
    private Long price;
    
    @NotNull(message="vyberte soubor obrázku")
    private MultipartFile image;
    
    @EmptyListConstraint
    private List<String> kategories;

    public String getName() {
        return name;
    }

    public Long getBrand() {
        return brand;
    }

    public String getDescription() {
        return description;
    }

    public Long getPrice() {
        return price;
    }

    public MultipartFile  getImage() {
        return image;
    }

    public List<String> getKategories() {
        return kategories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(Long brand) {
        this.brand = brand;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public void setKategories(List<String> kategories) {
        this.kategories = kategories;
    }    
}
