package com.finalproject.euromobil.site.controllers.admin;

import com.finalproject.euromobil.site.entities.Roles;
import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.forms.UserEditForm;
import com.finalproject.euromobil.site.forms.UserForm;
import com.finalproject.euromobil.site.forms.filterforms.UserFilterForm;
import com.finalproject.euromobil.site.services.UserService;
import java.util.HashSet;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/admin/users")
public class UserController 
{
    @Inject
    private UserService userService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
            
    @GetMapping({"/", ""})
    public String users(Model model, 
            Pageable pageable, 
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String login,
            @RequestParam(required = false) String authority,
            HttpSession session
    )
            
    {
        UserFilterForm userFilterForm = new UserFilterForm();
        userFilterForm.setSearch(name);
        userFilterForm.setUsername(login);
        userFilterForm.setAuthority(authority);

        boolean showResetButton =
                authority != null
                || !StringUtils.isEmpty(name)
                || !StringUtils.isEmpty(login);
        model.addAttribute("showResetButton", showResetButton);
        
        model.addAttribute("userFilterForm", userFilterForm);
        model.addAttribute("users", userService.findFiltered(pageable, name, login, authority));
        
        model.addAttribute("login", login);
        model.addAttribute("name", name);
        model.addAttribute("authority", authority);
        
        session.setAttribute("pageNum", pageable.getPageNumber());
        session.setAttribute("login", login);
        session.setAttribute("name", name);
        session.setAttribute("authority", authority);
        
        return "/users/users";
    }
    
    @PostMapping({"/", ""})
    public String usersFilter(UserFilterForm userFilterForm, RedirectAttributes redirectAttributes)
    {
        if(!StringUtils.isEmpty(userFilterForm.getAuthority()))
        {
            redirectAttributes.addAttribute("authority", userFilterForm.getAuthority());
        }
        if(!StringUtils.isEmpty(userFilterForm.getSearch()))
        {
            redirectAttributes.addAttribute("name", userFilterForm.getSearch());
        }
        if(!StringUtils.isEmpty(userFilterForm.getUsername()))
        {
            redirectAttributes.addAttribute("login", userFilterForm.getUsername());
        }
        
        return "redirect:/admin/users";
    }
    
    @GetMapping("/add-user")
    public String addUserForm(Model model)
    {
        model.addAttribute("userForm", new UserForm());
        return "/users/add-user";
    }
    
    @PostMapping("/add-user")
    public String addUser(@Valid UserForm userForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session)
    {      
        if(!userForm.getPassword().equals(userForm.getPasswordCheck()))
        {
            bindingResult.rejectValue("passwordCheck", "error.user", "Hesla se neshodují"); //tohle tam bylo pred tim, nez jsem objevil moznost pridat field error, ale libi se mi to
        }
        if(userService.existsByLogin(userForm.getLogin()))
        {
            bindingResult.addError(new FieldError("userForm", "login", "Uživatel s tímto loginem už existuje"));
        }
        
        if (bindingResult.hasErrors()) 
        {
            return "/users/add-user";
        }

        try
        {
        User u = new User();
        u.setName(userForm.getName());
        u.setLogin(userForm.getLogin());
        u.setPassword(passwordEncoder.encode(userForm.getPassword()));
        u.setAdress(userForm.getAdress());
        u.setIsEnabled(1L);
        
        Roles r = new Roles();
        r.setAuthority(userForm.getRole());
        r.setUser(u);
        
        u.setRoles(new HashSet<>());
        u.getRoles().add(r);
        userService.save(u);
        
        redirectAttributes.addFlashAttribute("successMessage", "Uživatel " + u.getLogin() + " byl úspěšně přidán");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Uživatele se nepodařilo přidat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("login", session.getAttribute("login"));
        redirectAttributes.addAttribute("name", session.getAttribute("name"));
        redirectAttributes.addAttribute("authority", session.getAttribute("authority"));
                
        return "redirect:/admin/users";
    }
    
    @GetMapping("/edit-user/{userId}")
    public String editUserForm(@PathVariable int userId, Model model, RedirectAttributes redirectAttributes)
    {
        if(!userService.findById(userId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Uživatel s id " + userId + " nebyl nalezen");
            return "redirect:/admin/users";
        }
        
        UserEditForm userEditForm = new UserEditForm();
        User u = userService.findById(userId).get();
        
        userEditForm.setName(u.getName());
        userEditForm.setLogin(u.getLogin());
        userEditForm.setAdress(u.getAdress());
        userEditForm.setRole(u.getRoles().iterator().next().getAuthority());
        
        model.addAttribute("editing", true);
        model.addAttribute("userEditForm", userEditForm);
        model.addAttribute("userId", userId);

        return "/users/edit-user";
    }
    
    @PostMapping("/edit-user/{userId}")
    public String editUser(@PathVariable int userId,@Valid UserEditForm userEditForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(userEditForm.getPassword() != null && !userEditForm.getPassword().equals("") && !userEditForm.getPassword().equals(userEditForm.getPasswordCheck()))
        {
            bindingResult.rejectValue("passwordCheck", "error.user", "Hesla se neshodují");
        }
        
        if (bindingResult.hasErrors()) 
        {
            return "/users/edit-user";
        }
        
        try
        {
        User u = userService.findById(userId).get();
        
        u.setName(userEditForm.getName());
        u.setLogin(userEditForm.getLogin());
        u.setAdress(userEditForm.getAdress());
        
        u.getRoles().iterator().next().setAuthority(userEditForm.getRole());
        
        if(userEditForm.getPassword() != null && !userEditForm.getPassword().equals(""))
        {
            u.setPassword(passwordEncoder.encode(userEditForm.getPassword()));
        }
        
        userService.save(u);
        
        redirectAttributes.addFlashAttribute("successMessage", "Uživatel s id " + u.getId() + " byl upraven");
        }
        catch(DataIntegrityViolationException ex)
        {
            bindingResult.addError(new FieldError("userEditForm", "login", "Uživatel s tímto loginem už existuje"));
            
            return "/users/edit-user";
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Uživatele se nepodařilo upravit");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("login", session.getAttribute("login"));
        redirectAttributes.addAttribute("name", session.getAttribute("name"));
        redirectAttributes.addAttribute("authority", session.getAttribute("authority"));
        
        return "redirect:/admin/users";
    }
    
    @GetMapping("/delete-user/{userId}")
    public View deleteUser(@PathVariable int userId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!userService.findById(userId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Uživatel s id " + userId + " nebyl nalezen");
            return new RedirectView("/admin/users");
        }
        
        try
        {
            userService.deleteById(userId);
            
            redirectAttributes.addFlashAttribute("successMessage", "Uživatel s id " + userId + " byl smazán");
        }
        catch(ConstraintViolationException | DataIntegrityViolationException ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Na uživateli s id " + userId + " je závislá objednávka nebo položky v košíku");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Uživatele se nepodařilo smazat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("login", session.getAttribute("login"));
        redirectAttributes.addAttribute("name", session.getAttribute("name"));
        redirectAttributes.addAttribute("authority", session.getAttribute("authority"));
        
        return new RedirectView("/admin/users");
    }
    
    @GetMapping("/enable-user/{userId}")
    public View setUserIsEnabled(@PathVariable int userId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!userService.findById(userId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Uživatel s id " + userId + " nebyl nalezen");
            
            redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
            redirectAttributes.addAttribute("login", session.getAttribute("login"));
            redirectAttributes.addAttribute("name", session.getAttribute("name"));
            redirectAttributes.addAttribute("authority", session.getAttribute("authority"));
        
            return new RedirectView("/admin/users");
        }
        
        try
        {
            
        
        User u = userService.findById(userId).get();
        String messgeActive;

        if(u.getIsEnabled() == 0)
        {
            u.setIsEnabled(1L);
            messgeActive = "aktivován";
        }
        else
        {
            u.setIsEnabled(0L);
            messgeActive = "deaktivován";
        }
        
        userService.save(u);
        
        redirectAttributes.addFlashAttribute("successMessage", "Uživatel s id " + userId + " byl " + messgeActive);
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Uživatele se nepodařilo aktivovat/deaktivovat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("login", session.getAttribute("login"));
        redirectAttributes.addAttribute("name", session.getAttribute("name"));
        redirectAttributes.addAttribute("authority", session.getAttribute("authority"));
        
        return new RedirectView("/admin/users");
    }
}
