package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Kategory;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KategoryRepository extends CrudRepository<Kategory, Long>
{
    @Override
    List<Kategory> findAll();
}
