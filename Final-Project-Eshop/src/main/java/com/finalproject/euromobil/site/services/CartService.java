package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Cart;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.forms.CartForm;
import java.util.List;
import java.util.Optional;

public interface CartService 
{
    List<Cart> findByUserId(Long userId);
    
    Optional<Cart> findById(Long cartId);
    
    void delete(Cart c);
    
    void deleteAllByUserId(Long userId);
    
    Cart create(CartForm cartForm, Goods goods, User user);
}
