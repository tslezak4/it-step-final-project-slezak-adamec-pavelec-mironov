package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Brand;
import java.util.List;
import java.util.Optional;

public interface BrandService 
{
    List<Brand> findAll();
    
    Optional<Brand> findById(long id);
    
    Brand save(Brand b);
    
    void deleteById(long id) throws Exception;
    
    boolean existsByName(String name);
}
