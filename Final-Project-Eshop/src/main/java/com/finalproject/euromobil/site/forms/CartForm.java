package com.finalproject.euromobil.site.forms;

import org.hibernate.validator.constraints.Range;

public class CartForm 
{
    @Range(min=1, max=10)
    private Long goodsQuantity;

    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }    
}
