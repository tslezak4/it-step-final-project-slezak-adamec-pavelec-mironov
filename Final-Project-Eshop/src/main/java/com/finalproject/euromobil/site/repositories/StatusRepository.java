package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Status;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status, Long>
{
    
}
