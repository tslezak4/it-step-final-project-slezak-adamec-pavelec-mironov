package com.finalproject.euromobil.site.services;

public interface EmailService 
{
    void sendSimpleMessage(String text, String to, String subject);
}
