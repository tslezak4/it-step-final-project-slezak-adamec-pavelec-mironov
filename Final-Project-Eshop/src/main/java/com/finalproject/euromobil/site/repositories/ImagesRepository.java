package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Images;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImagesRepository extends PagingAndSortingRepository<Images, Long>
{
    @Override
    Optional<Images> findById(Long id);

    @Override
    Images save(Images image);
}

