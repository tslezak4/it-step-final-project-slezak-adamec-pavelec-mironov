package com.finalproject.euromobil.site.exceptions;

public class UnsupportedImageTypeException extends Exception
{
      public UnsupportedImageTypeException() {
        super();
    }
    
    public UnsupportedImageTypeException(String message) {
        super(message);
    }  
}
