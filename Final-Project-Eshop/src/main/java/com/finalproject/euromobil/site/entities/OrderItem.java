package com.finalproject.euromobil.site.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="order_item")
public class OrderItem {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="goods_id", nullable=false)
    private Goods goods;
    
    @ManyToOne
    @JoinColumn(name="order_id", nullable=false)
    private Order order;
    
    @Column
    private Long fixedPrice;
    
    @Column
    private Long goodsQuantity;

    public Long getId() {
        return id;
    }

    public Goods getGoods() {
        return goods;
    }

    public Order getOrder() {
        return order;
    }

    public Long getFixedPrice() {
        return fixedPrice;
    }

    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setFixedPrice(Long fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }    
}
