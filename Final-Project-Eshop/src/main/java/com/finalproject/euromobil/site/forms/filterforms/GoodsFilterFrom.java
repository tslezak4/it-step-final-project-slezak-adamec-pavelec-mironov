package com.finalproject.euromobil.site.forms.filterforms;

import java.util.Set;

public class GoodsFilterFrom 
{
    String sort;
    String direction;
    Set<Long> kategory;
    Set<Long> brand;
    String search;

    public String getSort() {
        return sort;
    }

    public String getDirection() {
        return direction;
    }

    public Set<Long> getKategory() {
        return kategory;
    }

    public Set<Long> getBrand() {
        return brand;
    }

    public String getSearch() {
        return search;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setKategory(Set<Long> kategory) {
        this.kategory = kategory;
    }

    public void setBrand(Set<Long> brand) {
        this.brand = brand;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    
}
