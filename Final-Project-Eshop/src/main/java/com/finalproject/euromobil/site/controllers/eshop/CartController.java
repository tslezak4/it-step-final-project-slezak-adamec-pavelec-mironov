package com.finalproject.euromobil.site.controllers.eshop;

import com.finalproject.euromobil.site.entities.Cart;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.exceptions.CartEmptyException;
import com.finalproject.euromobil.site.forms.CartForm;
import com.finalproject.euromobil.site.services.CartService;
import com.finalproject.euromobil.site.services.GoodsService;
import com.finalproject.euromobil.site.services.OrderService;
import com.finalproject.euromobil.site.services.UserService;
import java.security.Principal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/cart")
public class CartController 
{
    @Inject
    private UserService userService;
    
    @Inject
    private CartService cartService;
    
    @Inject
    private GoodsService goodsService;
    
    @Inject
    private OrderService orderService;
    
    @GetMapping({"", "/"})
    public String cart(Model model, Principal principal)
    {
        User u = userService.findByLogin(principal.getName());
        List<Cart> cartItems = cartService.findByUserId(u.getId());

        
        int totalPrice = 0;
        for(Cart cartItem : cartItems)
        {
            totalPrice += cartItem.getGood().getPrice() * cartItem.getGoodsQuantity();
        }
        
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("carts", cartItems);
        
        return "/site/cart";
    }
    
    @GetMapping("/delete/{cartId}")
    public String deleteInCart(@PathVariable Long cartId, RedirectAttributes redirectAttributes)
    {
      if(!cartService.findById(cartId).isPresent())
      {
          redirectAttributes.addFlashAttribute("notFoundMessage", "položka s id " + cartId + " nebyla nalezena");
          
          return "redirect:/cart";
      }
      
      cartService.delete(cartService.findById(cartId).get());
      
      return "redirect:/cart";
    }
    
    @GetMapping("/cart-goods/{goodsId}")
    public String getCartGoods(@PathVariable long goodsId, Model model, RedirectAttributes redirectAttributes)
    { 
        if(!goodsService.findById(goodsId).isPresent() || !goodsService.findById(goodsId).get().getIsEnabled())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Zboží s id " + goodsId + " nebylo nalezeno");
            
            return "redirect:/";
        }
        
        Goods goods = goodsService.findById(goodsId).get();
        CartForm cartForm = new CartForm();
             
        model.addAttribute("cartForm", cartForm);
        
        model.addAttribute("goods", goods);
        
        return "site/cart-goods";
    }
    
    @PostMapping("/cart-goods/{goodsId}")
    public String addCartGoods(@PathVariable long goodsId, CartForm cartForm, Principal principal, Model model, RedirectAttributes redirectAttributes, HttpSession session)
    { 
        Goods article = goodsService.findById(goodsId).get(); 
        String username = principal.getName();       
        
        User user = userService.findByLogin(username); 
        Cart cart = this.cartService.create(cartForm, article, user);
        redirectAttributes.addFlashAttribute("successMessage", "Produkt " + article.getName() + " byl přidán do košíku");
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("kategoryIds", session.getAttribute("kategoryIds"));
        redirectAttributes.addAttribute("brandIds", session.getAttribute("brandIds"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/products";        
    }  
    
    @GetMapping("/submit-order")
    public String submitOrder(Principal principal, RedirectAttributes redirectAttributes)
    {
        
        try
        {
            orderService.create(new Order(), principal);
            
            cartService
                    .deleteAllByUserId(
                            userService
                                    .findByLogin(principal.getName())
                                    .getId()); 
            
            redirectAttributes.addFlashAttribute("successMessage", "Objednávka byla úspěšně odeslána");
        }
        catch(CartEmptyException ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Nelze odeslat objednávku - košík je prázdný");
            return "redirect:/cart";
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            
            redirectAttributes.addFlashAttribute("errorMessage", "Objednávku se nepodařilo odeslat");
            return "redirect:/cart";
        } 
        
        
        return "redirect:/";
    }
}


