package com.finalproject.euromobil.site.controllers.admin;

import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.forms.filterforms.OrderFilterForm;
import com.finalproject.euromobil.site.services.OrderItemService;
import com.finalproject.euromobil.site.services.OrderService;
import com.finalproject.euromobil.site.services.StatusService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/orders")
public class OrdersController 
{    
    @Inject
    private OrderService orderService;
    
    @Inject
    private OrderItemService orderItemService;
    
    @Inject
    private StatusService statusService;
    
    @GetMapping({"/", ""})
    public String orders(Model model,
            Pageable pageable,
            HttpSession session,
            @RequestParam(required = false) LocalDate dateFrom,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) LocalDate dateTo,
            @RequestParam(required = false) Long statusId,
            @RequestParam(required = false) String search)
    {
        boolean showResetButton =
                (dateFrom != null)
                || dateTo != null
                || !StringUtils.isEmpty(search)
                || !StringUtils.isEmpty(sort)
                || (statusId != null);
        model.addAttribute("showResetButton", showResetButton);
        
        Page<Order> orders = orderService.findFiltered(pageable, dateFrom, dateTo, statusId, search);
        OrderFilterForm orderFilterForm = new OrderFilterForm();
        
        if(dateFrom != null)
        {
            orderFilterForm.setDateFrom(dateFrom.toString());
            model.addAttribute("dateFrom", dateFrom.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        }
        if(dateTo != null)
        {
            orderFilterForm.setDateTo(dateTo.toString());
            model.addAttribute("dateTo", dateTo.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        }
        if(statusId != null)
        {
            orderFilterForm.setStatus(statusId);
            model.addAttribute("statusId", statusId);
        }
        if(!StringUtils.isEmpty(search))
        {
            orderFilterForm.setSearch(search);
            model.addAttribute("search", search);
        }
        if(!StringUtils.isEmpty(sort))
        {
            String[] sortDirection = sort.split(",");
            
            orderFilterForm.setSort(sortDirection[0]);
            orderFilterForm.setDirection(sortDirection[1]);
        }
        
        model.addAttribute("orders", orders);
        model.addAttribute("orderFilterForm", orderFilterForm);
 
        model.addAttribute("sort", orders
                .getSort()
                .get()
                .map(s -> s.getProperty() + "," + s.getDirection().name().toLowerCase())
                .findFirst()
                .orElse(null));
        
        session.setAttribute("pageNum", pageable.getPageNumber());
        session.setAttribute("sort", sort);
        session.setAttribute("dateFrom", dateFrom);
        session.setAttribute("dateTo", dateTo);
        session.setAttribute("statusId", statusId);
        session.setAttribute("search", search);
        
        return "/orders/orders";
    }
    
    @PostMapping({"", "/"})
    public String filter(OrderFilterForm orderFilterForm, RedirectAttributes redirectAttributes)
    {
        if(orderService.isDateValid(orderFilterForm.getDateFrom()))
        {
            LocalDate dateFrom = LocalDate.parse(orderFilterForm.getDateFrom());
            redirectAttributes.addAttribute("dateFrom", dateFrom);
        }
        if(orderService.isDateValid(orderFilterForm.getDateTo()))
        {
            LocalDate dateTo = LocalDate.parse(orderFilterForm.getDateTo());
            redirectAttributes.addAttribute("dateTo", dateTo);
        }
 
        redirectAttributes.addAttribute("statusId", orderFilterForm.getStatus());
        redirectAttributes.addAttribute("search", orderFilterForm.getSearch());
        
        String sort = orderFilterForm.getSort();
        if(!StringUtils.isEmpty(sort)) 
        {
            redirectAttributes.addAttribute("sort", sort + "," + orderFilterForm.getDirection());
        }
        
        return "redirect:/admin/orders";
    }
    
    @GetMapping("/detail/{orderId}")
    public String ordersDetail(Map<String, Object> model,@PathVariable int orderId)
    {        
        model.put("orderItems", orderItemService.findAllForOrder(orderId));
        return "/orders/order-detail";
    }
    
    @GetMapping("/enable-order/{orderId}/{statusId}")
    public String enableOrder(@PathVariable Long orderId, @PathVariable Long statusId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!orderService.findById(orderId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Objednávka s id " + orderId + " nebyla nalezena");
        }
        if(!statusService.findById(statusId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "status s id " + statusId + " nebyl nalezen");
        }
        
        try
        {
            Order o = orderService.findById(orderId).get();
            o.setStatus(statusService.findById(statusId).get());
            orderService.save(o);
            
            redirectAttributes.addFlashAttribute("successMessage", "Status objednávky " + orderId +" byl změněn");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Status objednávky se nepodařilo změnit");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("dateFrom", session.getAttribute("dateFrom"));
        redirectAttributes.addAttribute("dateTo", session.getAttribute("dateTo"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("statusId", session.getAttribute("statusId"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/orders";
    }
    
    @GetMapping("/delete-order/{orderId}")
    public String deleteOrder(@PathVariable Long orderId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!orderService.findById(orderId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Objednávka s id " + orderId + " nebyla nalezena");
        }
        
        try
        {
            orderItemService.deleteAllByOrderId(orderId);
            orderService.deleteById(orderId);
            
            redirectAttributes.addFlashAttribute("successMessage", "Objednávka byla úspěšně smazána");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            redirectAttributes.addFlashAttribute("errorMessage", "Objednávku se nepodařilo smazat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("dateFrom", session.getAttribute("dateFrom"));
        redirectAttributes.addAttribute("dateTo", session.getAttribute("dateTo"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("statusId", session.getAttribute("statusId"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/orders";
    }
}
