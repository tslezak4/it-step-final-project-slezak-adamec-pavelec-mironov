package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Cart;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long>
{

    List<Cart> findByUserId(Long userId);
    
    Optional<Cart> findByGoodIdAndUserId(Long goodsId, Long userId);
    
    @Override
    Optional<Cart> findById(Long cartId);
    
    @Override
    Cart save(Cart c);
    
    void deleteAllByUserId(Long userId);
}
