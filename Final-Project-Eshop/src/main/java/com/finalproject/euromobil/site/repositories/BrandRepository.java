package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Brand;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface BrandRepository extends CrudRepository<Brand, Long>
{
    @Override
    List<Brand> findAll();
    
    Optional<Brand> findById(long id);
    
    @Override
    Brand save(Brand b);
    
    boolean existsByName(String name);
}
