package com.finalproject.euromobil.site.controllers.eshop;

import com.finalproject.euromobil.config.CaptchaSettings;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.entities.Images;
import com.finalproject.euromobil.site.exceptions.ImageNotFoundException;
import com.finalproject.euromobil.site.services.GoodsService;
import com.finalproject.euromobil.site.forms.MessageForm;
import com.finalproject.euromobil.site.forms.filterforms.GoodsFilterFrom;
import com.finalproject.euromobil.site.json.CaptchaResponse;
import com.finalproject.euromobil.site.services.BrandService;
import com.finalproject.euromobil.site.services.EmailService;
import com.finalproject.euromobil.site.services.KategoryService;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SiteController 
{
    @Inject
    private GoodsService goodsService;
    
    @Inject
    private EmailService emailService;
    
    @Inject
    private KategoryService kategoryService;
    
    @Inject
    private BrandService brandService;
    
    @Inject
    private CaptchaSettings captchaSettings; 
    
    @Inject
    private RestTemplate restTemplate;
    
    @GetMapping({"/", "/products"})
    public String title(Model model, 
            Pageable pageable, 
            HttpSession session,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) Set<Long> kategoryIds,
            @RequestParam(required = false) Set<Long> brandIds,
            @RequestParam(required = false) String search)
    {
        boolean showResetButton =
                (kategoryIds != null && !kategoryIds.isEmpty())
                || !StringUtils.isEmpty(search)
                || !StringUtils.isEmpty(sort)
                || (brandIds != null && !brandIds.isEmpty());
        model.addAttribute("showResetButton", showResetButton);
        
        Page<Goods> page = goodsService.findFiltered(kategoryIds, brandIds, search, pageable, true);
        
        GoodsFilterFrom goodsFilterFrom = new GoodsFilterFrom();
        if(!StringUtils.isEmpty(sort))
        { 
            String[] sortDirection = sort.split(",");
            
            goodsFilterFrom.setSort(sortDirection[0]);
            goodsFilterFrom.setDirection(sortDirection[1]);
        }
        if(kategoryIds != null && !kategoryIds.isEmpty())
        {
            goodsFilterFrom.setKategory(kategoryIds);
            
            StringJoiner kategoryJoiner = new StringJoiner(",");
            kategoryIds.forEach(id -> {
                kategoryJoiner.add(id.toString());
            });
            model.addAttribute("kategoryIds", kategoryJoiner.toString());
        }
        else
        {
            model.addAttribute("kategoryIds", "");
        }
        if(brandIds != null && !brandIds.isEmpty())
        {
            goodsFilterFrom.setBrand(brandIds);
            
            StringJoiner brandJoiner = new StringJoiner(",");
            brandIds.forEach(id -> {
                brandJoiner.add(id.toString());
            });
            model.addAttribute("brandIds", brandJoiner.toString());
        }
        else
        {
             model.addAttribute("brandIds", "");
        }
        if(!StringUtils.isEmpty(search))
        {
            goodsFilterFrom.setSearch(search);
        }
        
        List<Goods> goods = page.getContent();
        
        int rows = goods.size() / 5;
        if(goods.size() % 5 != 0)
        {
            rows++;
        }
        
        model.addAttribute("goodsFilterForm", goodsFilterFrom);
        model.addAttribute("kategories", kategoryService.findAll());
        model.addAttribute("titles", goodsService.titleParts(goods));
        model.addAttribute("brands", brandService.findAll());
        model.addAttribute("rowCount", rows);
        model.addAttribute("goods", page);
        
        model.addAttribute("search", search);
        model.addAttribute("sort", page
                .getSort()
                .get()
                .map(s -> s.getProperty() + "," + s.getDirection().name().toLowerCase())
                .findFirst()
                .orElse(null));
        
        session.setAttribute("pageNum", pageable.getPageNumber());
        session.setAttribute("sort", sort);
        session.setAttribute("kategoryIds", kategoryIds);
        session.setAttribute("brandIds", brandIds);
        session.setAttribute("search", search);
        
        return "/site/title-products";
    }
    
    @PostMapping("/")
    public String filterGoods(GoodsFilterFrom goodsFilterFrom, RedirectAttributes redirectAttributes)
    {
        String sort = goodsFilterFrom.getSort();
        String direction = goodsFilterFrom.getDirection();
        Set<Long> kategoryIds = goodsFilterFrom.getKategory();
        Set<Long> brandIds = goodsFilterFrom.getBrand();
        String search = goodsFilterFrom.getSearch();
        
        if(!StringUtils.isEmpty(sort))
        {
            redirectAttributes.addAttribute("sort", sort + "," + direction);
        }
        if(kategoryIds != null && !kategoryIds.isEmpty())
        {
            redirectAttributes.addAttribute("kategoryIds", kategoryIds);
        }
        if(brandIds != null && !brandIds.isEmpty())
        {
            redirectAttributes.addAttribute("brandIds", brandIds);
        }
        if(!StringUtils.isEmpty(search))
        {
            redirectAttributes.addAttribute("search", search);
        }
        
        return "redirect:/";
    }
    
    @GetMapping("/contact")
    public String contact(Model model)
    {
        model.addAttribute("messageForm", new MessageForm());
        
        return "/site/contact";
    }
    
    @PostMapping("/contact")
    public String messageSending(@Valid MessageForm messageForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model,
            @RequestParam(name = "g-recaptcha-response") String reCaptchaResponse)
    {
        if(bindingResult.hasErrors())
        {
            return "/site/contact";
        }
        
        String url = captchaSettings.getUrl() + "?secret=" + captchaSettings.getSecret() + "&response=" + reCaptchaResponse;
        CaptchaResponse captchaResponse = restTemplate.exchange(url, HttpMethod.POST, null, CaptchaResponse.class).getBody();
        if(!captchaResponse.isSuccess())
        {
            model.addAttribute("errorMessage", "Chyba při zpracování reCaptcha");
            return "/site/contact";
        }
        
        try
        {
            emailService.sendSimpleMessage("Zpráva: " + messageForm.getMessage(), "euromobil.it.step@gmail.com", "Zpráva od " + messageForm.getEmail());
            emailService.sendSimpleMessage("Dobrý den," + System.lineSeparator() + "Přijali jsme Vaši zprávu z webu euromobil. odpovíme Vám co nejdříve." + System.lineSeparator() + "S pozdravem, team Euromobil ", messageForm.getEmail(), "Zpráva z webu euromobil.cz");
            
            redirectAttributes.addFlashAttribute("successMessage", "Zpráva byla úspěšně odeslána");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Zprávu se nepodařilo odeslat");
            return"redirect:/contact";
        }
        
        return"redirect:/contact";
    }
    
    @RequestMapping(value = "/detail/{productId}", method = RequestMethod.GET)
    public String detail(@PathVariable int productId, Model model, RedirectAttributes redirectAttributes)
    {
        if(!goodsService.findById(productId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Produkt s id " + productId + " nebyl nalezen");
            
            return "redirect:/";
        }
        
        Goods product = goodsService.findById(productId).get();
        List<String> partedDescription = Arrays.asList(product.getDescription().split(",\\s|•\\s"));
        
        model.addAttribute("partedDescription", partedDescription);
        model.addAttribute("product", product);
        
        return "/site/detail";
    }
    
    @RequestMapping("/login")
    public String login(@RequestParam(required=false) boolean error, @RequestParam(required=false) boolean logout, Model model)
    {
        if(error)
            model.addAttribute("errorMessage", "Nesprávné uživatelské jméno nebo heslo");
        
        if(logout)
            model.addAttribute("successMessage", "Byli jste úspěšně odhlášeni");
        
        return "/site/login";
    }
    
    @RequestMapping("/image/{goodsId}")
    public ResponseEntity<byte[]> image(@PathVariable("goodsId") Long goodsId)
    {
        Optional<Goods> optionalGoods = this.goodsService.findById(goodsId);
        if (!optionalGoods.isPresent())
            throw new ImageNotFoundException();
           

        Images picture = optionalGoods.get().getImages();

        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentType(MediaType.parseMediaType(picture.getContentType()));
        headers.setContentLength(picture.getContent().length);
        return new ResponseEntity<>(picture.getContent(), headers, HttpStatus.OK);
    }
    
}
