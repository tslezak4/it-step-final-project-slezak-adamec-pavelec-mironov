package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Brand;
import com.finalproject.euromobil.site.repositories.BrandRepository;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class DefaultBrandService implements BrandService
{
    @Inject
    private BrandRepository brandRepository;
    
    @Override
    public List<Brand> findAll() 
    {
        return brandRepository.findAll();
    } 
    
    @Override
    public Brand save(Brand b)
    {
        return brandRepository.save(b); 
    }
    
    @Override
    public Optional<Brand> findById(long id)
    {
        return brandRepository.findById(id);
    }
    
    @Override
    public void deleteById(long id) throws Exception
    {
        brandRepository.deleteById(id);
    }
    
    @Override
    public boolean existsByName(String name)
    {
        return brandRepository.existsByName(name);
    }
}
