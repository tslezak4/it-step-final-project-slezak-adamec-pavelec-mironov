package com.finalproject.euromobil.site.validation.validators;

import com.finalproject.euromobil.site.validation.annotations.NameConstraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<NameConstraint, String>
{
     @Override
    public void initialize(NameConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(String input, ConstraintValidatorContext cvc) {
        return input != null
                && input.matches("^[\\p{L} .'-]+$");
    }

}
