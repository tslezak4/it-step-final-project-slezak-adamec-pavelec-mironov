package com.finalproject.euromobil.site.controllers.admin;

import com.finalproject.euromobil.site.entities.Log;
import com.finalproject.euromobil.site.services.LogService;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin")
public class DashboardController 
{
    @Inject
    LogService logService;
    
    @GetMapping({"", "/", "/dashboard"})
    public String dashboard(Model model, Pageable pageable)
    {
        Page<Log> page = logService.findAll(pageable);
        
        if(page.isEmpty())
        {
            model.addAttribute("notFoundMessage", "Log je prázdný");
        }
        
        model.addAttribute("page", page);
        
        return "/dashboard";
    }
    
    @GetMapping("/log/delete-whole")
    public String deleteAll(RedirectAttributes redirectAttributes)
    {
        logService.deleteAll();
        redirectAttributes.addFlashAttribute("successMessage", "Všechny záznamy byly smazány");
        
        return "redirect:/admin";
    }
    
    @GetMapping("/log/delete/{logId}")
    public String deleteOne(@PathVariable Long logId, RedirectAttributes redirectAttributes)
    {
        logService.deleteById(logId);
        redirectAttributes.addFlashAttribute("successMessage", "Záznam s id "+ logId +" byl smazán");
        
        return "redirect:/admin";
    }
}
