package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.dto.GoodsTitleDTO;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.exceptions.UnsupportedImageTypeException;
import com.finalproject.euromobil.site.forms.GoodsForm;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GoodsService 
{
    Optional<Goods> findById(long id);
    Page<Goods> findAll(Pageable pageable);
    Page<Goods> findFiltered(Set<Long> kategoryIds, Set<Long> brandIds, String search, Pageable pageable, boolean filterEnabled);

    List<GoodsTitleDTO> titleParts(List<Goods> goods);
    void deleteById(long id);
    void enableGoods(long id);
    
    Goods create(GoodsForm goodsForm) throws IOException, UnsupportedImageTypeException;
    GoodsForm fillForm(Goods g);
    Goods edit(Goods g, GoodsForm goodsForm) throws IOException, UnsupportedImageTypeException;
}
