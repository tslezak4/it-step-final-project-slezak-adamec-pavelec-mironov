package com.finalproject.euromobil.site.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="goods")
public class Goods {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private String name;
    
    @ManyToOne
    @JoinColumn(name="brand_id", nullable=false)
    private Brand brand;
    
    @Column
    private String description;
    
    @Column
    private Long price;
    
    @Column
    private boolean isEnabled;
        
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="image_id", nullable=false)
    private Images images;    
    
    @ManyToMany
    @JoinTable(
        name = "goods_kategories", 
        joinColumns = @JoinColumn(name = "goods_id"), 
        inverseJoinColumns = @JoinColumn(name = "kategory_id"))
    private List<Kategory> kategories;

    public List<Kategory> getKetegories() {
        return kategories;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Brand getBrand() {
        return brand;
    }

    public String getDescription() {
        return description;
    }

    public Long getPrice() {
        return price;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public Images getImages() {
        return images;
    }

    public void setKategories(List<Kategory> kategories) {
        this.kategories = kategories;
    }    
}
