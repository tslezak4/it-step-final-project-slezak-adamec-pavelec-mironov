package com.finalproject.euromobil.site.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="kategories")
public class Kategory {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private String name;
    
    @ManyToMany
    @JoinTable(
        name = "goods_kategories", 
        joinColumns = @JoinColumn(name = "kategory_id"), 
        inverseJoinColumns = @JoinColumn(name = "goods_id"))
    private List<Goods> goods;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Goods> getGoods() {
        return goods;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }
    
}
