package com.finalproject.euromobil.site.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cart")
public class Cart {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="goods_id", nullable=false)
    private Goods good;
    
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;
    
    @Column
    private Long goodsQuantity;

    public Long getId() {
        return id;
    }

    public Goods getGood() {
        return good;
    }

    public User getUser() {
        return user;
    }

    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGood(Goods good) {
        this.good = good;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }   
}
