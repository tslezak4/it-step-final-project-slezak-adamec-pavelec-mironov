
package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Cart;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.forms.CartForm;
import com.finalproject.euromobil.site.repositories.CartRepository;
import com.finalproject.euromobil.site.repositories.UserRepository;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DefaultCartService implements CartService
{
    @Inject
    private CartRepository cartRepository;
    
    @Inject
    private UserRepository userRepository;
    
    @Override
    public List<Cart> findByUserId(Long userId) 
    {
        return cartRepository.findByUserId(userId);
    }
    
    @Override
    public Optional<Cart> findById(Long cartId)
    {
        return cartRepository.findById(cartId);
    }

    @Override
    @Transactional
    public Cart create(CartForm cartForm, Goods goods, User user)
    {
        if(cartRepository.findByGoodIdAndUserId(goods.getId(), user.getId()).isPresent())
        {
            Cart c = cartRepository.findByGoodIdAndUserId(goods.getId(), user.getId()).get();
            c.setGoodsQuantity(c.getGoodsQuantity() + cartForm.getGoodsQuantity());

            return this.cartRepository.save(c);
        }
        
        Cart newCart = new Cart();
        newCart.setGood(goods);
        newCart.setGoodsQuantity(cartForm.getGoodsQuantity());
        newCart.setUser(user);
              
        return this.cartRepository.save(newCart);
    }
    
    @Override
    public void delete(Cart c)
    {
        cartRepository.delete(c);
    }
    
    @Override
    @Transactional
    public void deleteAllByUserId(Long userId)
    {
        cartRepository.deleteAllByUserId(userId);
    }
            
        
}
