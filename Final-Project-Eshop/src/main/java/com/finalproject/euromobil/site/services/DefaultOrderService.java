package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.exceptions.CartEmptyException;
import com.finalproject.euromobil.site.repositories.CartRepository;
import com.finalproject.euromobil.site.repositories.OrderRepository;
import com.finalproject.euromobil.site.repositories.StatusRepository;
import com.finalproject.euromobil.site.repositories.UserRepository;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class DefaultOrderService implements OrderService
{
    @Inject
    private OrderRepository orderRepository;
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private StatusRepository statusRepository;
    
    @Inject
    private OrderItemService orderItemService;
    
    @Inject
    private CartRepository cartRepository;
    
    @Override
    public Page<Order> findFiltered(Pageable pageable, LocalDate dateFrom, LocalDate dateTo, Long statusId, String search)
    {
        Specification<Order> specification = null;
        
        if(dateFrom != null)
        {
            specification = Specification.where(fromDate(dateFrom));
        }
        
        if(dateTo != null)
        {
            specification = (specification == null)
                    ? Specification.where(toDate(dateTo))
                    : specification.and(toDate(dateTo));
        }
        
        if(statusId != null)
        {
            specification = (specification == null)
                    ? Specification.where(hasStatus(statusId))
                    : specification.and(hasStatus(statusId));
        }
        
        if(!StringUtils.isEmpty(search))
        {
            try
            {
                Long orderId = Long.parseLong(search);
                
                specification = (specification == null)
                    ? Specification.where(hasId(orderId))
                    : specification.and(hasId(orderId));
            }
            catch(NumberFormatException e)
            {
                specification = (specification == null)
                    ? Specification.where(UserNameContains(search))
                    : specification.and(UserNameContains(search));
            }
        }
     
        return orderRepository.findAll(specification, pageable);
    }
    
    static Specification<Order> fromDate(LocalDate dateFrom)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("dateOrdered"), dateFrom);
    }
    
    static Specification<Order> toDate(LocalDate dateTo)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("dateOrdered"), dateTo);
    }
    
    static Specification<Order> hasStatus(Long statusId)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("status").get("id"), statusId);
    }
    
    static Specification<Order> UserNameContains(String name)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("user").get("name"),"%" + name + "%");
    }
    
    static Specification<Order> hasId(Long orderId)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), orderId);
    }
    
    @Override
    public boolean isDateValid(String localDate)
    {
        try
        {
            LocalDate.parse(localDate);
        }
        catch(DateTimeParseException e)
        {
            return false;
        }
        return true;
    }
    
    @Override
    public Order save(Order o)
    {
        return orderRepository.save(o);
    }
    
    @Override
    public void deleteById(Long orderId)
    {
        orderRepository.deleteById(orderId);
    }
    
    @Override
    @Transactional
    public Order create(Order o, Principal principal) throws CartEmptyException
    {
        User logged = userRepository.findByLogin(principal.getName());
        
        if(cartRepository.findByUserId(logged.getId()).isEmpty())
        {
            throw new CartEmptyException();
        }
        
        o.setUser(logged);
        o.setStatus(statusRepository.findById(1L).get());
        o.setDateOrdered(LocalDate.now());
        
        o = orderRepository.save(o);
        
        orderItemService.createFromCart(o, logged.getId());
        
        return o;
    }
    
    @Override
    public Optional<Order> findById(Long orderId)
    {
        return orderRepository.findById(orderId);
    }
}