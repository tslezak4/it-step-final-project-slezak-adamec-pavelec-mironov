package com.finalproject.euromobil.site.dto;

import java.util.List;

public class GoodsTitleDTO 
{
    Long id;
    List<String> titleParts;

    public Long getId() {
        return id;
    }

    public List<String> getTitleParts() {
        return titleParts;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitleParts(List<String> titleParts) {
        this.titleParts = titleParts;
    }
    
    
}
