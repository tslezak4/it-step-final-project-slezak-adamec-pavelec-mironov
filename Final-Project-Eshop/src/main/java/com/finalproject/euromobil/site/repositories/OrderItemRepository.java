package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.OrderItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long>
{
    //vím, že to jde jednodušeji, ale chtěl jsem si to zkusit
    @Query(value = "SELECT * FROM order_item as oi JOIN orders as o ON o.id = oi.order_id AND oi.order_id = ?1", nativeQuery = true) 
    List<OrderItem> findAllForOrder(int orderId);
    
    @Override
    OrderItem save(OrderItem oi);
    
    void deleteAllByOrderId(Long orderId);
}
