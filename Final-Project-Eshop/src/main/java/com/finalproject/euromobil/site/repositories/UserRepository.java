package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User>
{
    @Override
    Page<User> findAll(Specification<User> specification ,Pageable pageable);
    
    @Override
    User save(User u);

    Optional<User> findById(long id);
    
    User findByLogin(String login);
    
    void deleteById(long id);
    
    boolean existsByLogin(String login);
}
