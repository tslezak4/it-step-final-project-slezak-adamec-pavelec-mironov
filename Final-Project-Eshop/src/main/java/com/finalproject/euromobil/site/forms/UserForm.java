package com.finalproject.euromobil.site.forms;

import com.finalproject.euromobil.site.validation.annotations.NameConstraint;
import com.finalproject.euromobil.site.validation.annotations.ValidPassword;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class UserForm 
{
    @NotBlank
    @Size(min=4, max=60)
    @NameConstraint
    private String name;
    
    @NotBlank
    @Size(min=2, max=15)
    private String login;
    
    @ValidPassword
    private String password;
    
    @NotBlank
    private String passwordCheck;
    
    @NotBlank
    @Email
    private String adress;
    
    @NotNull
    private String role;

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public String getAdress() {
        return adress;
    }

    public String getRole() {
        return role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }
    

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
