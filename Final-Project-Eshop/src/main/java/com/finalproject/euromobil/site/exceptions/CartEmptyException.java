package com.finalproject.euromobil.site.exceptions;

public class CartEmptyException extends Exception
{
    public CartEmptyException()
    {
        super();
    }
    
    public CartEmptyException(String message)
    {
        super(message);
    }
}
