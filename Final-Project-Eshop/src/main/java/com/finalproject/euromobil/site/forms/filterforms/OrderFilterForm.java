package com.finalproject.euromobil.site.forms.filterforms;


public class OrderFilterForm 
{
    String sort;
    String direction;
    String dateFrom;
    String dateTo;
    Long status;
    String search;

    public String getSort() {
        return sort;
    }

    public String getDirection() {
        return direction;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public Long getStatus() {
        return status;
    }

    public String getSearch() {
        return search;
    }
    
    public void setSort(String sort) {
        this.sort = sort;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
