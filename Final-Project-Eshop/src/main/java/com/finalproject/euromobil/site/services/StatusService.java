package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Status;
import java.util.Optional;

public interface StatusService 
{
    Optional<Status> findById(Long statusId);
}
