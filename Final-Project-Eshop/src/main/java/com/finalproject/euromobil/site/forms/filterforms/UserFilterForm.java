package com.finalproject.euromobil.site.forms.filterforms;

public class UserFilterForm 
{
    String authority;
    String search;
    String username;

    public String getAuthority() {
        return authority;
    }

    public String getSearch() {
        return search;
    }

    public String getUsername() {
        return username;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    
}
