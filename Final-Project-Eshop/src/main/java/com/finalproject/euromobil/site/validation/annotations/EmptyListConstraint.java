package com.finalproject.euromobil.site.validation.annotations;

import com.finalproject.euromobil.site.validation.validators.EmptyListValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = EmptyListValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface EmptyListConstraint 
{
    String message() default "Vyberte alespoň jednu možnost";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
