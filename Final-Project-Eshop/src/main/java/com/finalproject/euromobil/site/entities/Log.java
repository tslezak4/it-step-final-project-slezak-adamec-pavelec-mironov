package com.finalproject.euromobil.site.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="log")
public class Log 
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column
    private String entity;
    
    @Column
    private String action;
    
    @Column
    private String message;
    
    @Column
    private Timestamp dateCreated;

    public Long getId() {
        return id;
    }

    public String getEntity() {
        return entity;
    }

    public String getAction() {
        return action;
    }

    public String getMessage() {
        return message;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    
}
