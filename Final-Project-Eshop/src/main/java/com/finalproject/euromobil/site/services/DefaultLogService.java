package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Log;
import com.finalproject.euromobil.site.repositories.LogRepository;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DefaultLogService implements LogService
{
    @Inject
    LogRepository logRepository;
    
    @Override
    public Page<Log> findAll(Pageable pageable) 
    {
        return logRepository.findAll(pageable);
    } 
    
    @Override
    public void deleteAll()
    {
        logRepository.deleteAll();
    }
    
    @Override
    public void deleteById(Long id)
    {
        logRepository.deleteById(id);
    }
}
