package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.User;
import com.finalproject.euromobil.site.repositories.RolesRepository;
import com.finalproject.euromobil.site.repositories.UserRepository;
import java.util.Optional;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@Transactional
public class DefaultUserService implements UserService
{
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private RolesRepository rolesRepository;
    
    @Override
    public Page<User> findAll(Pageable pageable) 
    {
        return userRepository.findAll(pageable);
    }  
    
    @Override
    public User save(User u)
    {
        return userRepository.save(u); 
    }
    
    @Override
    public Optional<User> findById(long id)
    {
        return userRepository.findById(id);
    }
    
    @Override
    public User findByLogin(String login)
    {
        return userRepository.findByLogin(login);
    }    
    
    @Override
    public void deleteById(long id)
    {
        userRepository.deleteById(id);
    }
    
    @Override
    public boolean existsByLogin(String login)
    {
        return userRepository.existsByLogin(login);
    }
    
    static Specification<User> nameContains(String name)
    {
        return (article, cq, cb) -> cb.like(article.get("name"), "%" + name + "%");
    }
    
    static Specification<User> loginContains(String login)
    {
        return (article, cq, cb) -> cb.like(article.get("login"), "%" + login + "%");
    }
    
    static Specification<User> hasRoleId(String authority)
    {
        return new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> article, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Join join = article.join("roles");
                return cb.like(join.get("authority"), authority);
            }
        };
    }
    
    @Override
    public Page<User> findFiltered(Pageable pageable, String name, String login, String authority)
    {
        Specification<User> specification = null;
        
        if (!StringUtils.isEmpty(name))
        {
            specification = Specification.where(nameContains(name));
        }
        
        if (!StringUtils.isEmpty(login))
        {
            specification = (specification == null)
                    ? Specification.where(loginContains(login))
                    : specification.and(loginContains(login));
        }
        
        if (!StringUtils.isEmpty(authority) && authority != null)
        {
            specification = (specification == null)
                    ? Specification.where(hasRoleId(authority))
                    : specification.and(hasRoleId(authority));
        }
        
        return userRepository.findAll(specification, pageable);
    }
}