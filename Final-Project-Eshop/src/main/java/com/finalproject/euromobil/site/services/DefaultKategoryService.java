package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Kategory;
import com.finalproject.euromobil.site.repositories.KategoryRepository;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class DefaultKategoryService implements KategoryService
{
    @Inject
    private KategoryRepository kategoryRepository;
    
    @Override
    public List<Kategory> findAll() 
    {
        return kategoryRepository.findAll();
    }  
}
