package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LogRepository extends PagingAndSortingRepository<Log, Long>
{
    @Override
    Page<Log> findAll(Pageable pageable);
}
