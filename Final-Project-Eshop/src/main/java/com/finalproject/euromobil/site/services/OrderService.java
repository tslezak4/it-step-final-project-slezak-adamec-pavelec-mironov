package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Order;
import com.finalproject.euromobil.site.exceptions.CartEmptyException;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService 
{
    Page<Order> findFiltered(Pageable pageable, LocalDate dateFrom, LocalDate dateTo, Long status, String search);
    
    Optional<Order> findById(Long orderId);
    
    Order save(Order o);
    
    void deleteById(Long orderId);
    
    Order create(Order o, Principal principal) throws CartEmptyException;
    
    boolean isDateValid(String localDate);
}
