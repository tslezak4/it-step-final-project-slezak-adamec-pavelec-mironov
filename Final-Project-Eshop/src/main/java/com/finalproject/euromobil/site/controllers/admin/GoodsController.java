package com.finalproject.euromobil.site.controllers.admin;

import com.finalproject.euromobil.site.entities.Brand;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.exceptions.UnsupportedImageTypeException;
import com.finalproject.euromobil.site.forms.GoodsForm;
import com.finalproject.euromobil.site.forms.filterforms.GoodsFilterFrom;
import com.finalproject.euromobil.site.services.BrandService;
import com.finalproject.euromobil.site.services.GoodsService;
import com.finalproject.euromobil.site.services.KategoryService;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/goods")
public class GoodsController 
{
    @Inject
    private GoodsService goodsService;
    
    @Inject
    private BrandService brandService;
    
    @Inject
    private KategoryService kategoryService;
    
    @GetMapping({"/", ""})
    public String goods(Model model, 
            Pageable pageable, 
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) Set<Long> kategoryIds,
            @RequestParam(required = false) Set<Long> brandIds,
            @RequestParam(required = false) String search,
            HttpSession session)
            
    {
        boolean showResetButton =
                (kategoryIds != null && !kategoryIds.isEmpty())
                || !StringUtils.isEmpty(search)
                || !StringUtils.isEmpty(sort)
                || (brandIds != null && !brandIds.isEmpty());
        model.addAttribute("showResetButton", showResetButton);
        
        Page<Goods> page = goodsService.findFiltered(kategoryIds, brandIds, search, pageable, false);
        GoodsFilterFrom goodsFilterFrom = new GoodsFilterFrom();
        if(!StringUtils.isEmpty(sort))
        { 
            String[] sortDirection = sort.split(",");
            
            goodsFilterFrom.setSort(sortDirection[0]);
            goodsFilterFrom.setDirection(sortDirection[1]);
        }
        if(kategoryIds != null && !kategoryIds.isEmpty())
        {
            goodsFilterFrom.setKategory(kategoryIds);
            
            StringJoiner kategoryJoiner = new StringJoiner(",");
            kategoryIds.forEach(id -> {
                kategoryJoiner.add(id.toString());
            });
            model.addAttribute("kategoryIds", kategoryJoiner.toString());
        }
        else
        {
            model.addAttribute("kategoryIds", "");
        }
        if(brandIds != null && !brandIds.isEmpty())
        {
            goodsFilterFrom.setBrand(brandIds);
            
            StringJoiner brandJoiner = new StringJoiner(",");
            brandIds.forEach(id -> {
                brandJoiner.add(id.toString());
            });
            model.addAttribute("brandIds", brandJoiner.toString());
        }
        else
        {
            model.addAttribute("brandIds", "");
        }
        if(!StringUtils.isEmpty(search))
        {
            goodsFilterFrom.setSearch(search);
        }

        model.addAttribute("goodsFilterForm", goodsFilterFrom);
        model.addAttribute("kategories", kategoryService.findAll());
        model.addAttribute("page", page);
        model.addAttribute("brands", brandService.findAll());
             
        model.addAttribute("search", search);
        model.addAttribute("sort", page
                .getSort()
                .get()
                .map(s -> s.getProperty() + "," + s.getDirection().name().toLowerCase())
                .findFirst()
                .orElse(null));
        
        session.setAttribute("pageNum", pageable.getPageNumber());
        session.setAttribute("sort", sort);
        session.setAttribute("kategoryIds", kategoryIds);
        session.setAttribute("brandIds", brandIds);
        session.setAttribute("search", search);
        
        return "/goods/goods";
    }
    
    @PostMapping("")
    public String filterGoods(GoodsFilterFrom goodsFilterFrom, RedirectAttributes redirectAttributes)
    {
        String sort = goodsFilterFrom.getSort();
        String direction = goodsFilterFrom.getDirection();
        Set<Long> kategoryIds = goodsFilterFrom.getKategory();
        Set<Long> brandIds = goodsFilterFrom.getBrand();
        String search = goodsFilterFrom.getSearch();
        
        if(!StringUtils.isEmpty(sort))
        {
            redirectAttributes.addAttribute("sort", sort + "," + direction);
        }
        if(kategoryIds != null && !kategoryIds.isEmpty())
        {
            redirectAttributes.addAttribute("kategoryIds", kategoryIds);
        }
        if(brandIds != null && !brandIds.isEmpty())
        {
            redirectAttributes.addAttribute("brandIds", brandIds);
        }
        if(!StringUtils.isEmpty(search))
        {
            redirectAttributes.addAttribute("search", search);
        }
        
        return "redirect:/admin/goods";
    }
    
    @RequestMapping(value="/add-goods", method=RequestMethod.GET)
    public String addForm(Model model) 
    {
        GoodsForm goodsForm = new GoodsForm();        
        model.addAttribute("goodsForm", goodsForm);
        
        List<Brand> brands = brandService.findAll();
        model.addAttribute("brands", brands);
        
        return "/goods/add-goods";
    }
    
    @RequestMapping(value="/add-goods", method=RequestMethod.POST) 
    public String add(@Valid GoodsForm goodsForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpSession session) throws IOException
    {
        if (bindingResult.hasErrors())
        {
            model.addAttribute("brands", brandService.findAll());
            return "/goods/add-goods";
        }

        try
        {
            Goods goods = this.goodsService.create(goodsForm);
            redirectAttributes.addFlashAttribute("successMessage", "Produkt " + goods.getName() + " Byl úspěšně uložen");
        }
        catch(UnsupportedImageTypeException e)
        {
            model.addAttribute("brands", brandService.findAll());
            
            bindingResult.addError(new FieldError("goodsForm", "image", e.getMessage()));
            return "/goods/add-goods";
        }
        catch(Exception ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Produkt se nepodařilo přidat. Pravděpodobně už existuje");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("kategoryIds", session.getAttribute("kategoryIds"));
        redirectAttributes.addAttribute("brandIds", session.getAttribute("brandIds"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/goods";
    }
    
    @GetMapping("/edit-goods/{goodsId}")
    public String getEditGoods(@PathVariable long goodsId, Model model, RedirectAttributes redirectAttributes)
    { 
        if(!goodsService.findById(goodsId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Produkt s id " + goodsId + " neexistuje");
        }
        
        Goods g = goodsService.findById(goodsId).get();
        GoodsForm goodsForm = goodsService.fillForm(g);
        
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("brands", brandService.findAll());
        model.addAttribute("goodsId", goodsId);
        
        return "/goods/edit-goods";
    }
    
    @PostMapping("/edit-goods/{goodsId}")
    public String editGoods(@Valid GoodsForm goodsForm, BindingResult bindingResult, @PathVariable long goodsId, Model model, RedirectAttributes redirectAttributes, HttpSession session) throws IOException
    {
        if (bindingResult.hasErrors())
        {
            model.addAttribute("brands", brandService.findAll());
            return "/goods/edit-goods";
        }

        try
        {
            Goods goods = goodsService.edit(goodsService.findById(goodsId).get(), goodsForm);
            
            redirectAttributes.addFlashAttribute("successMessage", "Produkt s id " + goodsId + " byl upraven");
        }
        catch(UnsupportedImageTypeException e)
        {
            model.addAttribute("brands", brandService.findAll());
            bindingResult.addError(new FieldError("goodsForm", "image", "Nepodporovaný typ obrázku"));
            return "/goods/edit-goods";
        }
        catch(Exception ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Produkt s id " + goodsId + " se nepodařilo upravit");  
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("kategoryIds", session.getAttribute("kategoryIds"));
        redirectAttributes.addAttribute("brandIds", session.getAttribute("brandIds"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/goods";
    }
    
    @GetMapping("/delete-goods/{goodsId}")
    public String deleteGoods(@PathVariable long goodsId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!goodsService.findById(goodsId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Produkt s id " + goodsId + " nebyl nalezen");
            
            return "redirect:/admin/goods";
        }
        
        try
        {
        Goods g = goodsService.findById(goodsId).get();
        goodsService.deleteById(goodsId);
        
        redirectAttributes.addFlashAttribute("successMessage", "Produkt s id " + goodsId + " byl úspěšně smazán");
        }
        catch(ConstraintViolationException | DataIntegrityViolationException ex)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Na pruduktu s id " + goodsId + " je závislá objednávka nebo položky v košíku");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Produkt s id " + goodsId + " se nepodařilo smazat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("kategoryIds", session.getAttribute("kategoryIds"));
        redirectAttributes.addAttribute("brandIds", session.getAttribute("brandIds"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/goods";
    }
    
    @GetMapping("/enable-goods/{goodsId}")
    public String enable(@PathVariable long goodsId, RedirectAttributes redirectAttributes, HttpSession session)
    {
        if(!goodsService.findById(goodsId).isPresent())
        {
            redirectAttributes.addFlashAttribute("notFoundMessage", "Produkt s id " + goodsId + " nebyl nalezen");
            
            return "redirect:/admin/goods";
        }
        
        try
        {
            goodsService.enableGoods(goodsId);
        
            redirectAttributes.addFlashAttribute("successMessage", "Produkt s id " + goodsId + " byl aktivován/deaktivován");
        }
        catch(Exception e)
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Produkt s id" + goodsId + "se nepodařilo aktivovst/deaktivovat");
        }
        
        redirectAttributes.addAttribute("page", session.getAttribute("pageNum"));
        redirectAttributes.addAttribute("sort", session.getAttribute("sort"));
        redirectAttributes.addAttribute("kategoryIds", session.getAttribute("kategoryIds"));
        redirectAttributes.addAttribute("brandIds", session.getAttribute("brandIds"));
        redirectAttributes.addAttribute("search", session.getAttribute("search"));
        
        return "redirect:/admin/goods";
    }
}
