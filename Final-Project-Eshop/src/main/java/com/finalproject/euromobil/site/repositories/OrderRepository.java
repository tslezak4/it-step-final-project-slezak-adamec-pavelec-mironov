package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>, JpaSpecificationExecutor<Order>
{
    @Override
    Page<Order> findAll(Specification<Order> specification, Pageable pageable);
    
    @Override
    Order save(Order o);
}
