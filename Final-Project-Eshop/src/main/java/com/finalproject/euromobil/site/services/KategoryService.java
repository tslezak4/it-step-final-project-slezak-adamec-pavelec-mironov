package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Kategory;
import java.util.List;

public interface KategoryService 
{
    List<Kategory> findAll();
}
