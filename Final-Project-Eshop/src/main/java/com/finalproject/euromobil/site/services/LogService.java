package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.entities.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LogService 
{
    Page<Log> findAll(Pageable pageable);
    void deleteAll();
    void deleteById(Long id);
}
