package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.repositories.GoodsRepository;
import com.finalproject.euromobil.site.repositories.ImagesRepository;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class DefaultImagesService implements ImagesService
{
    @Inject
    private ImagesRepository imagesRepository;

    @Inject
    private GoodsRepository goodsRepository;
}