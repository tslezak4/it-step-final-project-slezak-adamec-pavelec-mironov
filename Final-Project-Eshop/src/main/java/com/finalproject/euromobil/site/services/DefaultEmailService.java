package com.finalproject.euromobil.site.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class DefaultEmailService implements EmailService
{
    
    @Autowired
    private JavaMailSender emailSender;

    @Override
    public void sendSimpleMessage(String text, String to, String subject) 
    {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setFrom("mailService@euromobil.cz");
        message.setTo(to); 
        message.setSubject(subject); 
        message.setText(text);
        emailSender.send(message);
    }
    
}
