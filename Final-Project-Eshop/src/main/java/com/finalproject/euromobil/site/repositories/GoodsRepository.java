package com.finalproject.euromobil.site.repositories;

import com.finalproject.euromobil.site.entities.Goods;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsRepository extends PagingAndSortingRepository<Goods, Long>, JpaSpecificationExecutor<Goods>
{
    @Override
    Page<Goods> findAll(Specification<Goods> specification, Pageable pageable);
    
    Optional<Goods> findById(long id);
    
    @Override
    Goods save(Goods s);
    
    void deleteById(long id);
}
