package com.finalproject.euromobil.site.entities;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private String name;
    
    @Column
    private String login;
    
    @Column
    private String password;
    
    @Column
    private Long isEnabled;
    
    @Column
    private String adress;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="user")
    private Set<Roles> roles;

    public Set<Roles> getRoles() {
        return roles;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Long getIsEnabled() {
        return isEnabled;
    }

    public String getAdress() {
        return adress;
    }
    
    public void setRoles(Set<Roles> roles) {
    this.roles = roles;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIsEnabled(Long isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
    
    
}
