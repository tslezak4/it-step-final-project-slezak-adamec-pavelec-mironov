package com.finalproject.euromobil.site.validation.validators;

import com.finalproject.euromobil.site.validation.annotations.EmptyListConstraint;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmptyListValidator implements ConstraintValidator<EmptyListConstraint, List<String>>
{

    @Override
    public void initialize(EmptyListConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(List<String> k, ConstraintValidatorContext cvc) {
        
        return !k.isEmpty();
    }
    
}
