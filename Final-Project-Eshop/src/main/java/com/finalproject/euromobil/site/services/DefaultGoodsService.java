
package com.finalproject.euromobil.site.services;

import com.finalproject.euromobil.site.dto.GoodsTitleDTO;
import com.finalproject.euromobil.site.entities.Goods;
import com.finalproject.euromobil.site.entities.Images;
import com.finalproject.euromobil.site.entities.Kategory;
import com.finalproject.euromobil.site.exceptions.UnsupportedImageTypeException;
import com.finalproject.euromobil.site.forms.GoodsForm;
import com.finalproject.euromobil.site.repositories.GoodsRepository;
import com.finalproject.euromobil.site.repositories.ImagesRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class DefaultGoodsService implements GoodsService
{
    @Inject
    private GoodsRepository goodsRepository;
    
    @Inject
    private ImagesRepository imagesRepository;
    
    @Inject
    private BrandService brandService;
    
    @Inject
    private KategoryService kategoryService;
    
    @Override
    public Page<Goods> findAll(Pageable pageable) 
    {
        return goodsRepository.findAll(pageable);
    } 
    @Override
    public Optional<Goods> findById(long id)
    {
        return goodsRepository.findById(id);
    }
         
    @Override
    public List<GoodsTitleDTO> titleParts(List<Goods> goods)
    {
        List<GoodsTitleDTO> titles = new ArrayList<>();

        for(Goods one : goods)
        {
            GoodsTitleDTO dto = new GoodsTitleDTO();
            List<String> partedTitle = new ArrayList<>();
            
            String titleForPartition = one.getName();
            int thirdSpaceIndex = ordinalIndexOf(titleForPartition, " ", 3);
            
            if(thirdSpaceIndex != -1)
            {
                partedTitle.add(titleForPartition.substring(0, thirdSpaceIndex));
                partedTitle.add(titleForPartition.substring(thirdSpaceIndex));
            }
            else
            {
                partedTitle.add(titleForPartition);
                partedTitle.add("");
            }
            
            dto.setId(one.getId());
            dto.setTitleParts(partedTitle);
            
            titles.add(dto);
        }
        return titles;
    }
    
    //tohle je metoda na vyhledani n-teho indexu substrinu ve stringu. Je potreba na deleni pro nadpis
    public static int ordinalIndexOf(String str, String substr, int n) 
    {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }
    
    @Override
    @Transactional
    public Goods create(GoodsForm goodsForm) throws IOException, UnsupportedImageTypeException
    {
        if (!goodsForm.getImage().getContentType().equals("image/jpeg")){
            
            if(goodsForm.getImage().isEmpty()){
                throw new UnsupportedImageTypeException("Vložte obrázek");
            }
            
            throw new UnsupportedImageTypeException("Nepodporovany typ obrazku");
        }

        Images images = new Images();
        images.setName(goodsForm.getImage().getOriginalFilename());
        images.setContentType(goodsForm.getImage().getContentType());
        images.setContent(goodsForm.getImage().getBytes());
        images = this.imagesRepository.save(images);

        Goods newProduct = new Goods();
        newProduct.setName(goodsForm.getName());
        newProduct.setDescription(goodsForm.getDescription());
        newProduct.setPrice(goodsForm.getPrice());
        newProduct.setIsEnabled(true);
        newProduct.setImages(images);
        
        newProduct.setBrand(brandService.findById(goodsForm.getBrand()).get());
                
        List<Kategory> kategories = kategoryService.findAll();
        List<String> kategorySelect = goodsForm.getKategories();
        kategories.removeIf(k -> !kategorySelect.contains(k.getName())); //TODO pres hibernate repozitar
        newProduct.setKategories(kategories);        
        
        return this.goodsRepository.save(newProduct);
    }   
    
    @Override
    public GoodsForm fillForm(Goods g)
    {
        GoodsForm goodsForm = new GoodsForm();
        
        goodsForm.setName(g.getName());
        goodsForm.setDescription(g.getDescription());
        goodsForm.setPrice(g.getPrice());
        
        List<String> kategories = new ArrayList<>();
        g.getKetegories().forEach((kategory) -> {
            kategories.add(kategory.getName());
        });
        
        goodsForm.setKategories(kategories);
        goodsForm.setBrand(g.getBrand().getId());
        
        return goodsForm;
    }
    
    @Override
    @Transactional
    public Goods edit(Goods g, GoodsForm goodsForm) throws IOException, UnsupportedImageTypeException
    {
        g.setName(goodsForm.getName());
        g.setDescription(goodsForm.getDescription());
        g.setPrice(goodsForm.getPrice());
        g.setBrand(brandService.findById(goodsForm.getBrand()).get());
        
        List<Kategory> kategories = kategoryService.findAll();
        List<String> kategorySelect = goodsForm.getKategories();
        kategories.removeIf(k -> !kategorySelect.contains(k.getName())); //TODO pres hibernate repozitar
        g.setKategories(kategories);
        
        if(goodsForm.getImage() != null && !goodsForm.getImage().isEmpty())
        {
            if (!goodsForm.getImage().getContentType().equals("image/jpeg"))
            throw new UnsupportedImageTypeException("Nepodporovany typ obrazku");

            Images i = g.getImages();
            i.setName(goodsForm.getImage().getOriginalFilename());
            i.setContentType(goodsForm.getImage().getContentType());
            i.setContent(goodsForm.getImage().getBytes());
            i = this.imagesRepository.save(i);
            g.setImages(i);
        }

        return goodsRepository.save(g);
    }
    
    @Override
    @Transactional
    public void deleteById(long id)
    {
        Goods g = this.findById(id).get();
        goodsRepository.deleteById(g.getId());
        imagesRepository.deleteById(g.getImages().getId());
    }
    
    @Override
    public void enableGoods(long id)
    {
        Goods g = this.findById(id).get();
        
        if(g.getIsEnabled())
        {
            g.setIsEnabled(false);
        }
        else g.setIsEnabled(true);
        
        goodsRepository.save(g);
    }
    
    static Specification<Goods> searchContains(String search)
    {
        return (article, cq, cb) -> cb.like(article.get("name"), "%" + search + "%");
    }
    
    static Specification<Goods> hasBrandIds(Set<Long> brandIds)
    {
        return (root, criteriaQuery, criteriaBuilder) -> root.get("brand").in(brandIds);
    }
    
    static Specification<Goods> isEnabled(boolean enabled)
    {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("isEnabled"), enabled);
    }
    
    static Specification<Goods> hasKategoryId(Set<Long> kategoryIds)
    {
        
        return (root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.distinct(true);
            Root<Kategory> kategories = criteriaQuery.from(Kategory.class);
            Expression<Collection<Goods>> goods = kategories.get("goods");
            return criteriaBuilder.and(kategories.in(kategoryIds), criteriaBuilder.isMember(root, goods));
        };

        
        
    }

    
    @Override
    public Page<Goods> findFiltered(Set<Long> kategoryIds, Set<Long> brandIds, String search, Pageable pageable, boolean filterEnabled)
    {
        Specification<Goods> specification = null;
        
        if (!StringUtils.isEmpty(search))
        {
            specification = Specification.where(searchContains(search));
        }
        
        if(brandIds != null && !brandIds.isEmpty())
        {
            specification = (specification == null)
                    ? Specification.where(hasBrandIds(brandIds))
                    : specification.and(hasBrandIds(brandIds));
        }
        
        if (kategoryIds != null && !kategoryIds.isEmpty())
        {
            specification = (specification == null)
                    ? Specification.where(hasKategoryId(kategoryIds))
                    : specification.and(hasKategoryId(kategoryIds));
        }
        
        if(filterEnabled)
        {
            specification = (specification == null)
                ? Specification.where(isEnabled(true))
                : specification.and(isEnabled(true));
        }
        
        
        return goodsRepository.findAll(specification, pageable);
    }
}
