package com.finalproject.euromobil.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class DatabaseSecurityConfiguration extends WebSecurityConfigurerAdapter
{
    
    @Autowired
    private DataSource dataSource;
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
         auth
            .jdbcAuthentication()
            .dataSource(dataSource)
            .passwordEncoder(passwordEncoder())
            
            .usersByUsernameQuery(
                "SELECT login as username, password as password, is_enabled as enabled " // vyzadovany nazvy: username, password, enabled 
                    + "FROM users "
                    + "WHERE login = ?")
            
            .authoritiesByUsernameQuery(
                "SELECT u.login as username, a.authority as role " // vyzadovany nazvy: username, role
                    + "FROM roles a, users u " 
                    + "WHERE u.login = ? " 
                    + "AND u.id = a.user_id");
                
    }
            
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.formLogin();
        http
                .authorizeRequests()
                .antMatchers("/cart/**").hasAnyAuthority("admin", "uzivatel")
                .antMatchers("/admin/**").hasAuthority("admin")
                .antMatchers("/").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error=true")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .httpBasic()
                .and()
                .logout()
                .logoutSuccessUrl("/login?logout=true")
                .deleteCookies("JSESSIONID")
                .and()
                .rememberMe().key("rememberUniqueKey")
                .and()
                .csrf()
                .disable();
                
             
            
    }
    
    @Override
public void configure(WebSecurity web) throws Exception {
    web
            .ignoring()
            .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**");
}
}
