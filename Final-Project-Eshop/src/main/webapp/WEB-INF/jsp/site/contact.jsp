<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Kontakt</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/submitjs.js" charset="utf-8"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
    <c:set var="contact" value="true" />
  <%@include file="/WEB-INF/jspf/navbar.jspf" %>
<main>
<sec:authorize access="isAuthenticated()">
  <div class="section">
    <div class="chip right">
      <img src="img/user.png" alt="">
      <sec:authentication property="principal.username" />
    </div>
  </div>
  </sec:authorize>
    
    <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
    <c:if test="${errorMessage != null}">
          <%@include file="/WEB-INF/jspf/error-message.jspf" %>
      </c:if>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col s5">
                    <h4>Projekt Euromobil</h4>
                    <h5>Tvůrci webu:</h5>
                    <ul>
                        <li>Tomáš Slezák</li>
                        <li>Sergey Mironov</li>
                        <li>Martin Adamec</li>
                        <li>Šimon Pavelec</li>
                    </ul>
                    <br>
                    <h5>Telefon:</h5><p>+420 789 456 123</p>
                    <h5>Email:</h5><p>euromobil@gmail.com</p>
                    
                </div>
                <div class="col s5 offset-s1">
                    <div><iframe style="width: 450px; height: 450px" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=ko%20bulon%20le+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/draw-radius-circle-map/">km radius map</a></iframe></div>
                </div>
            </div>
            <div class="row">
                <h4 class="center-align">Napište nám</h4>
                <div class="col s8 offset-s3">
                    <form:form action="/contact" method="post" modelAttribute="messageForm" enctype="multipart/form-data">
                        <div class="row">
                            <div class="input-field col s4">
                                <form:input path="email"/>
                                <form:label path="email">Email</form:label>
                                <form:errors path="email" cssClass="red-text"/>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="input-field col s8">
                                <form:textarea path="message" class="materialize-textarea"></form:textarea>
                                <form:label path="message">Zpráva</form:label>
                                <form:errors path="message" cssClass="red-text"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s4 offset-s1">
                                <div data-sitekey="6LfWVboZAAAAAInpxxat81VEpbxUIxxUoavmz4qu" class="g-recaptcha "></div>
                                <span id="captchaError" style="display:none"></span>
                            </div>
                            
                            
                            <div class="col s4 offset-s2" id="submitDiv">
                                <button class="btn-large blue darken-2" type="submit" id="submitInput"><i class="material-icons right">send</i>Odeslat</button>
                            </div>
                        </div>
                        
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</main>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
