<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>
<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>košík</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/forModal.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
   <%@include file="/WEB-INF/jspf/modal-dialog.jspf" %>
   <c:set var="cart" value="true" />
   <%@include file="/WEB-INF/jspf/navbar.jspf" %>

  <div class="section">
    <div class="chip right">
      <img src="/img/user.png" alt="">
      <sec:authentication property="principal.username" />
    </div>
  </div>
    
    <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>

  <main>
      
      <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>
      
    <h3 class="center-align">Vaše objednávka</h3>

    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s12">
              <c:if test="${carts.size() == 0}">
                  <div class="section">
                    <div class="container">
                      <div class="row">
                        <div class="col s10 offset-s1 yellow lighten-3 center-align message-box">
                          <div class="text-bold message-content">Košík je prázdný</div>
                        </div>
                      </div>
                    </div>
                  </div>
              </c:if>
              <c:if test="${carts.size() > 0}">
            <table class="cart-table centered">
                      <thead>
                        <tr>
                            <th></th>
                            <th>Obsah košíku</th>
                            <th>Množství</th>
                            <th>Cena s DPH</th>
                            <th></th>
                        </tr>
                      </thead>

                      <tbody>
                        <c:forEach var="cartItem" items="${carts}">
                        <tr>
                            
                          <td> <a href="/detail/${cartItem.good.id}"><img src="image/${cartItem.good.id}" height="64px"> </a></td>
                          <td><a class="black-text" href="/detail/${cartItem.good.id}">${cartItem.good.name}</a></td>
                          <td>${cartItem.goodsQuantity}</td>
                          <td>${cartItem.good.price * cartItem.goodsQuantity} Kč</td>
                          <td><a href="#confirmation" 
                                class="modal-trigger btn blue darken-2"
                                data-modal-title="Odstranit položku"
                                data-modal-body="Opravdu chcete odebrat tento produkt z košíku?"
                                data-modal-success-title="Odstranit"
                                data-modal-success-class="red"
                                data-modal-redirect="/cart/delete/${cartItem.id}">
                                  <i class="material-icons">delete</i>
                              </a>
                          </td>
                        </tr>
                        </c:forEach>
                        <tr>
                          <td></td>
                          <td class="text-bold" >Celková cena s DPH</td>
                          <td></td>
                          <td class="text-bold" >${totalPrice} Kč</td>
                          <td></td>
                        </tr>
                      </tbody>
              </table>
              </c:if>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s6 offset-s9">
              <c:if test="${carts.size() != 0}">
                  <a href="#confirmation" 
                    class="modal-trigger btn-large blue darken-2"
                    data-modal-title="Odeslat objednávku"
                    data-modal-body="Opravdu si přejete odeslat objednávku?"
                    data-modal-success-title="Odeslat"
                    data-modal-success-class="blue"
                    data-modal-redirect="/cart/submit-order">
                    Odeslat objednávku
                  </a>
              </c:if>
          </div>
        </div>
      </div>
    </div>
  </main>


<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>

