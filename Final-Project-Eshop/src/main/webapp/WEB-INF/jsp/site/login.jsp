<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="/css/materialize.css">
    <link rel="stylesheet" href="/css/vlastni-styly.css">

    <script type="text/javascript"
    src="https://code.jquery.com/jquery-2.1.1.min.js">
    </script>
    <script src="/js/materialize.js" charset="utf-8"></script>
    <script src="/js/myjs.js" charset="utf-8"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Login</title>
  </head>

  <body>
       <c:set var="loginPage" value="true" />
    <%@include file="/WEB-INF/jspf/navbar.jspf" %>
    <main>
        
        <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
      <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>

      <div class="container">
    <div class="row">
        <div class="col s6 offset-s3">
            <h2 class="center-align">Login</h2>
            <div class="row">
                <form class="col s12" action="login" method="POST">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="username" name="username" type="text" class="validate">
                            <label for="username">Uživatelské jméno</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="password" name="password" type="password" class="validate">
                            <label for="password">Heslo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                          <p>
                            <label>
                              <input type="checkbox" name="remember-me"/>
                              <span>Zapamatovat</span>
                            </label>
                          </p>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col m12">
                            <p class="right-align">
                                <button class="btn btn-large waves-effect waves-light blue darken-2" type="submit" name="submit">Login</button>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    </main>
    <%@include file="/WEB-INF/jspf/footer.jspf" %>
  </body>
</html>

