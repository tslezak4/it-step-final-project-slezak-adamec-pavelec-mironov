<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Euromobil</title>

  <script src="/js/multiple-select-fix.js" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>


  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
  
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
  
</head>
<body>
    <c:set var="products" value="true" />
    <c:set var="colCounter" value="0" />
  <%@include file="/WEB-INF/jspf/navbar.jspf" %>

  <sec:authorize access="isAuthenticated()">
  <div class="section">
    <div class="chip right">
      <img src="img/user.png" alt="">
      <sec:authentication property="principal.username" />
    </div>
  </div>
  </sec:authorize>
  <main>
  <form:form method="post" action="/" modelAttribute="goodsFilterForm">
    <div class="container">
      <div class="section">
        <div class="row grey lighten-2 valign-wrapper">
          <div class="input-field col s2">
            <form:select path="sort">
              <form:option value="" disabled="true" selected="selected">Řadit podle</form:option>
              <form:option value="price">cena</form:option>
              <form:option value="name">jméno</form:option>
              <form:option value="brand">značka</form:option>
            </form:select>
            <form:label path="sort">Řazení</form:label>
          </div>
          <div class="input-field col s2">
            <form:select path="direction">
              <form:option value="asc" selected="selected">Vzestupně</form:option>
              <form:option value="desc">Sestupně</form:option>
            </form:select>
          </div>
            <div class="input-field col s2 offset-s1">
              <form:select path="kategory" multiple="true">
                <form:option value="" disabled="true" selected="selected">kategorie</form:option>
                
                <c:forEach var="kategory" items="${kategories}">
                <form:option value="${kategory.id}">${kategory.name}</form:option>
                </c:forEach>
                
              </form:select>
                <form:label style="margin-bottom: 20px" path="kategory">Filtry</form:label>
            </div>
          
            <div class="input-field col s2">
              <form:select path="brand" multiple="multiple">
                <form:option value="" disabled="true" selected="selected">značky</form:option>
                
                <c:forEach items="${brands}" var="brand"> 
                
                <form:option value="${brand.id}">${brand.name}</form:option>
                                
                </c:forEach>
                
                
              </form:select>
            </div>
            <div class="input-field col s2">
              <form:input path="search" id="searchfield" type="text" class="validate"/>
              <form:label path="search" for="searchfield">název</form:label>
            </div>
            <div class="col s1 valign">
              <button class="blue darken-2 btn waves-effect waves-light" type="submit" name="action">
                <i class="material-icons center">search</i>
              </button>
                
            </div>
            <c:if test="${showResetButton}">
            <div class="col s1">
                <a class="btn blue darken-2" href="/products">RESET</a>
            </div>
            </c:if>
      </div>
    </div>
  </div>
  </form:form>
  
    <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
    <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>
  
  
<div class="section">
    <c:if test="${showResetButton && goods.isEmpty()}">
        <c:set var="notFoundMessage" value="pro tento výběr nebylo nic nalezeno" />
        <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
    </c:if>
  <c:forEach begin="0" end="${rowCount}">
  <div class="row">
    <c:if test="${goods.getContent().size() > colCounter}">
    <div class="col s2 offset-s1">
      <div class="card large hoverable">
          <div class="card-title truncate own-title"><c:forEach items="${titles}" var="title"><c:if test="${goods.getContent().get(colCounter).id == title.id}">${title.titleParts.get(0)}<br>${title.titleParts.get(1)}</c:if></c:forEach></div>
        <div class="card-image">
          <a href="/detail/${goods.getContent().get(colCounter).id}"><img class="center-block goods-image" src="/image/${goods.getContent().get(colCounter).id}"></a>
          <span class="card-title blue darken-2 card-prices">${goods.getContent().get(colCounter).price} Kč</span>
        </div>
        <div class="card-content">
            <div class="card-div"><a class="black-text" href="/detail/${goods.getContent().get(colCounter).id}">${goods.getContent().get(colCounter).description}</a></div>
        </div>
        <div class="card-action">
          <a class="right" style="margin-right: 0px" href="/cart/cart-goods/${goods.getContent().get(colCounter).id}">Do košíku</a>
          <span class="green-text">skladem</span>
        </div>
      </div>
    </div>
    <c:set var="colCounter" value="${colCounter + 1}" />
    </c:if>

    <c:if test="${goods.getContent().size() > colCounter}">
    <div class="col s2">
      <div class="card large hoverable">
          <div class="card-title truncate own-title"><c:forEach items="${titles}" var="title"><c:if test="${goods.getContent().get(colCounter).id == title.id}">${title.titleParts.get(0)}<br>${title.titleParts.get(1)}</c:if></c:forEach></div>
        <div class="card-image">
          <a href="/detail/${goods.getContent().get(colCounter).id}"><img class="center-block goods-image" src="/image/${goods.getContent().get(colCounter).id}"></a>
          <span class="card-title blue darken-2 card-prices">${goods.getContent().get(colCounter).price} Kč</span>
        </div>
        <div class="card-content">
            <div class="card-div"><a class="black-text" href="/detail/${goods.getContent().get(colCounter).id}">${goods.getContent().get(colCounter).description}</a></div>
        </div>
        <div class="card-action">
          <a class="right" style="margin-right: 0px" href="/cart/cart-goods/${goods.getContent().get(colCounter).id}">Do košíku</a>
          <span class="green-text">skladem</span>
        </div>
      </div>
    </div>
    <c:set var="colCounter" value="${colCounter + 1}" />
    </c:if>

    <c:if test="${goods.getContent().size() > colCounter}">
    <div class="col s2">
      <div class="card large hoverable">
          <div class="card-title truncate own-title"><c:forEach items="${titles}" var="title"><c:if test="${goods.getContent().get(colCounter).id == title.id}">${title.titleParts.get(0)}<br>${title.titleParts.get(1)}</c:if></c:forEach></div>
        <div class="card-image">
          <a href="/detail/${goods.getContent().get(colCounter).id}"><img class="center-block goods-image" src="/image/${goods.getContent().get(colCounter).id}"></a>
          <span class="card-title blue darken-2 card-prices">${goods.getContent().get(colCounter).price} Kč</span>
        </div>
        <div class="card-content">
            <div class="card-div"><a class="black-text" href="/detail/${goods.getContent().get(colCounter).id}">${goods.getContent().get(colCounter).description}</a></div>
        </div>
        <div class="card-action">
          <a class="right" style="margin-right: 0px" href="/cart/cart-goods/${goods.getContent().get(colCounter).id}">Do košíku</a>
          <span class="green-text">skladem</span>
        </div>
      </div>
    </div>
    <c:set var="colCounter" value="${colCounter + 1}" />
    </c:if>

    <c:if test="${goods.getContent().size() > colCounter}">
    <div class="col s2">
      <div class="card large hoverable">
          <div class="card-title truncate own-title"><c:forEach items="${titles}" var="title"><c:if test="${goods.getContent().get(colCounter).id == title.id}">${title.titleParts.get(0)}<br>${title.titleParts.get(1)}</c:if></c:forEach></div>
        <div class="card-image">
          <a href="/detail/${goods.getContent().get(colCounter).id}"><img class="center-block goods-image" src="/image/${goods.getContent().get(colCounter).id}"></a>
          <span class="card-title blue darken-2 card-prices">${goods.getContent().get(colCounter).price} Kč</span>
        </div>
        <div class="card-content">
            <div class="card-div"><a class="black-text" href="/detail/${goods.getContent().get(colCounter).id}">${goods.getContent().get(colCounter).description}</a></div>
        </div>
        <div class="card-action">
          <a class="right" style="margin-right: 0px" href="/cart/cart-goods/${goods.getContent().get(colCounter).id}">Do košíku</a>
          <span class="green-text">skladem</span>
        </div>
      </div>
    </div>
    <c:set var="colCounter" value="${colCounter + 1}" />
    </c:if>

    <c:if test="${goods.getContent().size() > colCounter}">
    <div class="col s2">
      <div class="card large hoverable">
          <div class="card-title truncate own-title"><c:forEach items="${titles}" var="title"><c:if test="${goods.getContent().get(colCounter).id == title.id}">${title.titleParts.get(0)}<br>${title.titleParts.get(1)}</c:if></c:forEach></div>
        <div class="card-image">
          <a href="/detail/${goods.getContent().get(colCounter).id}"><img class="center-block goods-image" src="/image/${goods.getContent().get(colCounter).id}"></a>
          <span class="card-title blue darken-2 card-prices">${goods.getContent().get(colCounter).price} Kč</span>
        </div>
        <div class="card-content">
            <div class="card-div"><a class="black-text" href="/detail/${goods.getContent().get(colCounter).id}">${goods.getContent().get(colCounter).description}</a></div>
        </div>
        <div class="card-action">
          <a class="right" style="margin-right: 0px" href="/cart/cart-goods/${goods.getContent().get(colCounter).id}">Do košíku</a>
          <span class="green-text">skladem</span>
        </div>
      </div>
    </div>
    <c:set var="colCounter" value="${colCounter + 1}" />
    </c:if>
  </div>
  </c:forEach>
    
    <c:if test="${goods.pageable.pageNumber > goods.totalPages}">
        <c:set var="notFoundMessage" value="Strana ${goods.pageable.pageNumber} neexistuje" />
        <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
    </c:if>
  
</div>
<c:if test="${goods.totalPages > 1}">
  <div class="section">
  <div class="row">
    <div class="col s8 offset-s2">
      <ul class="pagination center">
          <li class="${goods.isFirst() ? "disabled" : ""}"><a<c:if test="${!goods.isFirst()}"> href="?page=${goods.previousOrFirstPageable().pageNumber}&sort=${sort}&kategoryIds=${kategoryIds}&brandIds=${brandIds}&search=${search}"</c:if>><i class="material-icons">chevron_left</i></a></li>
        <c:forEach begin="1" end="${goods.totalPages}" varStatus="pageIter">
        <li class="waves-effect ${goods.pageable.pageNumber == pageIter.index - 1 ? "active blue darken-2" : ""}"><a href="?page=${pageIter.index - 1}&sort=${sort}&kategoryIds=${kategoryIds}&brandIds=${brandIds}&search=${search}">${pageIter.index}</a></li>
        </c:forEach>
        <li class="${goods.isLast() ? "disabled" : ""}"><a<c:if test="${!goods.isLast()}"> href="?page=${goods.nextOrLastPageable().pageNumber}&sort=${sort}&kategoryIds=${kategoryIds}&brandIds=${brandIds}&search=${search}"</c:if>><i class="material-icons">chevron_right</i></a></li>
      </ul>
    </div>
  </div>
</div>
</c:if>
  </main>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
