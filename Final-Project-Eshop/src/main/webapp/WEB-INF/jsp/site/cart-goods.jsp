<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>
<c:set var="formTitle" value="Přidání zboží do košíku" />
<c:set var="formType" value="cartForm"/>
<c:set var="formAction" value="${goodsId}"/>
<%@include file="/WEB-INF/jspf/cart-form.jspf" %>
