<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>${product.name}</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="/js/myjs.js" charset="utf-8"></script>

    <link rel="stylesheet" href="/css/materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/css/vlastni-styly.css">
  </head>
  <body>
    <c:set var="products" value="true" />
      <%@include file="/WEB-INF/jspf/navbar.jspf" %>
    <main>
      <sec:authorize access="isAuthenticated()">
      <div class="section">
       <div class="chip right">
         <img src="/img/user.png" alt="">
         <sec:authentication property="principal.username" />
       </div>
      </div>
     </sec:authorize>

      <section class="zakladni-info">
        <div class="zakladni-info-img">
            <img class="detail-image" src="/image/${product.id}" alt="" />
        </div>
        <div class="zakladni-info-popis">
          <h2>
            ${product.name}
          </h2>
          <ul class="list-info">
            <c:forEach items="${partedDescription}" var="descItem">
                <li>${descItem}</li>
            </c:forEach>
          </ul>
          <div class="button-container">
            <h5 style="display: inline-block" class="">${product.price} Kč</h5>
            <a href="/cart/cart-goods/${product.id}" class="btn blue darken-2 detail-button">Vložit do košíku</a>
          </div>
        </div>
      </section>
    </main>
    <%@include file="/WEB-INF/jspf/footer.jspf" %>
  </body>
</html>
