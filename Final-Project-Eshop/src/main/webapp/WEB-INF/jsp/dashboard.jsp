<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Administrace</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
        <script src="/js/materialize.js"></script>
        <script src="/js/myjs.js" charset="utf-8"></script>

        <link rel="stylesheet" href="/css/materialize.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/css/vlastni-styly.css">
    </head>
    <body>
        <c:set var="forAdmins" value="true" />
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>
        <main>

        <div class="section">
            <div class="chip right">
                <img src="/img/user.png" alt="">
                <sec:authentication property="principal.username" />
            </div>
        </div>
            
        <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
         
        <div class="container">
            <div class="section">
                <h2 class="center-align">ADMINISTRACE</h2>
            </div>
            
            <div class="section">
                <div class="row">
                    <div class="col s12">
                        <h4>Log:</h4>
                        <br>
                        <a href="/admin/log/delete-whole" class="btn blue darken-2 <c:if test="${page.getContent() == null || page.getContent().isEmpty()}">disabled</c:if>">Smazat log</a>
                        <c:if test="${page.getContent() != null && !page.getContent().isEmpty()}">
                        <table class="centered highlight">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Entita</th>
                                    <th>Provedená akce</th>
                                    <th>Zpráva</th>
                                    <th>Datum</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach var="logItem" items="${page.getContent()}">
                                <tr>
                                    <td>${logItem.id}</td>
                                    <td>${logItem.entity}</td>
                                    <td>${logItem.action}</td>
                                    <td>${logItem.message}</td>
                                    <td>${logItem.dateCreated}</td>
                                    <td><a href="/admin/log/delete/${logItem.id}" class="blue-text text-darken-2"><i class="material-icons">delete</i></a></td>
                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        </c:if>
                        <c:if test="${page.getContent() == null || page.getContent().isEmpty()}">
                            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
                        </c:if>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col s12">
                        <h4>// Tady ještě něco bude</h4>
                    </div>
                </div>
            </div>
                        
            <div class="section">
             <div class="row">
              <div class="col s8 offset-s2">
                <ul class="pagination center">
                  <li class="${page.isFirst() ? "disabled" : ""}"><a<c:if test="${!page.isFirst()}"> href="?page=${page.previousOrFirstPageable().pageNumber}"</c:if>><i class="material-icons">chevron_left</i></a></li>
                    <c:forEach begin="1" end="${page.totalPages}" varStatus="pageIter">
                      <li class="waves-effect ${page.pageable.pageNumber == pageIter.index - 1 ? "active blue darken-2" : ""}"><a href="?page=${pageIter.index - 1}">${pageIter.index}</a></li>
                    </c:forEach>
                  <li class="${page.isLast() ? "disabled" : ""}"><a<c:if test="${!page.isLast()}"> href="?page=${page.nextOrLastPageable().pageNumber}"</c:if>><i class="material-icons">chevron_right</i></a></li>
                </ul>
              </div>
             </div>
            </div>   
                
        </div>    
        
        </main>
            
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>  
