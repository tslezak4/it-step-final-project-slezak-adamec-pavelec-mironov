<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Uživatelé</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/forModal.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
  <%@include file="/WEB-INF/jspf/modal-dialog.jspf" %>
  <c:set var="forAdmins" value="true" />
  <%@include file="/WEB-INF/jspf/navbar.jspf" %>
  <main>

    <div class="section">
      <div class="chip right">
        <img src="/img/user.png" alt="">
        <sec:authentication property="principal.username" />
      </div>
    </div>

    <div class="fixed-action-btn">
        <a href="users/add-user" class="btn-floating btn-large blue darken-4">
        <i class="large material-icons">add</i>
      </a>
    </div>
      
    <form:form action="/admin/users" modelAttribute="userFilterForm" id="user-filter-form" method="post">
      <div class="container">
      <div class="section">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="row grey lighten-2">
                   <div class="input-field col s3">
                    <form:select path="authority">
                      <form:option value="" selected="selected" disabled="true">role</form:option>
                      <form:option value="admin">Admin</form:option>
                      <form:option value="uzivatel">Uživatel</form:option>
                    </form:select>
                    <form:label path="authority">Filtry</form:label>
                  </div>
                  <div class="input-field col s3">
                    <form:input path="search" id="name-search-field" type="text"/>
                    <form:label path="search">jméno</form:label>
                  </div>
                  <div class="input-field col s3">
                    <form:input path="username" id="name-search-field" type="text"/>
                    <form:label path="username">uživatelské jméno</form:label>
                  </div>
                  <div class="col s1">
                      <button class="blue darken-2 btn waves-effect waves-light" style="margin-top: 25px" type="submit" name="action">
                      <i class="material-icons center">search</i>
                    </button>
                  </div>
                    <c:if test="${showResetButton}">
                        <div class="col s1">
                            <a href="/admin/users" class="blue darken-2 btn waves-effect waves-light" style="margin-top: 25px">RESET</a>
                        </div>
                    </c:if>
                </div>
            </div>
            
      </div>
    </div>
  </div>
      
      </form:form>
      
      <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
      <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>
      
      <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>

    <div class="container">
      <div class="section">
        <div class="row grey lighten-2">
          <div class="col s8 offset-s2">
              
              <c:if test="${showResetButton && users.isEmpty()}">
                <c:set var="notFoundMessage" value="pro tento výběr nebylo nic nalezeno" />
                <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
              </c:if>
              
            <ul class="collection">
            <c:forEach items="${users.content}" var="user">
              <li class="collection-item avatar">
                <img src="/img/user.png" alt="" class="circle">
                <span class="title text-bold">${user.name}</span>
                <p>login: ${user.login} <br>
                   email: ${user.adress}
                </p>
                <a href="users/edit-user/${user.id}" class="secondary-content blue-text text-darken-2 col-mar-15top col-mar-140right">Upravit</a>
                <a href="#confirmation"
                   class="modal-trigger secondary-content blue-text text-darken-2 col-mar-15top col-mar-100right"
                   data-modal-title="Odstranit uživatele"
                   data-modal-body="Opravdu chcete odstranit uživatele ${user.name}?"
                   data-modal-success-title="Odstranit"
                   data-modal-success-class="red"
                   data-modal-redirect="/admin/users/delete-user/${user.id}">
                    <i class="material-icons">delete</i>
                </a>
                <a href="users/enable-user/${user.id}" class="secondary-content ${user.isEnabled == 1 ? 'green-text' : 'red-text'} col-mar-15top">${user.isEnabled == 1 ? 'Aktivní' : 'Neaktivní'}</a>
              </li>
            </c:forEach>
            </ul>
          </div>
        </div>
      </div>
    </div>
      
      <c:if test="${users.pageable.pageNumber - 1 > users.totalPages}">
        <c:set var="notFoundMessage" value="Strana ${users.pageable.pageNumber} neexistuje" />
        <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
    </c:if>
      
<c:if test="${users.totalPages > 1}">
  <div class="section">
  <div class="row">
    <div class="col s8 offset-s2">
      <ul class="pagination center">
          <li class="${users.isFirst() ? "disabled" : ""}"><a<c:if test="${!users.isFirst()}"> href="?page=${users.previousOrFirstPageable().pageNumber}&name=${name}&login=${login}&authority=${authority}"</c:if>><i class="material-icons">chevron_left</i></a></li>
        <c:forEach begin="1" end="${users.totalPages}" varStatus="pageIter">
        <li class="waves-effect ${users.pageable.pageNumber == pageIter.index - 1 ? "active blue darken-2" : ""}"><a href="?page=${pageIter.index - 1}&name=${name}&login=${login}&authority=${authority}">${pageIter.index}</a></li>
        </c:forEach>
        <li class="${users.isLast() ? "disabled" : ""}"><a<c:if test="${!users.isLast()}"> href="?page=${users.nextOrLastPageable().pageNumber}&name=${name}&login=${login}&authority=${authority}"</c:if>><i class="material-icons">chevron_right</i></a></li>
      </ul>
    </div>
  </div>
</div>
</c:if>
  </main>


  <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
