<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<c:set var="formTitle" value="Upravit uživatele" />
<c:set var="formAction" value="${userId}"/>
<c:set var="formType" value="userEditForm"/>
<%@include file="/WEB-INF/jspf/user-form.jspf" %>