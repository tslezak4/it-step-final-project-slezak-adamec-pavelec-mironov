<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<c:set var="formTitle" value="Přidat uživatele" />
<c:set var="formAction" value="add-user"/>
<c:set var="formType" value="userForm"/>
<%@include file="/WEB-INF/jspf/user-form.jspf" %>

