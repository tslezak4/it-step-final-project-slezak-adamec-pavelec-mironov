<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/base.jspf" %>

<html lang="cs">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Detail</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="/js/myjs.js" charset="utf-8"></script>

    <link rel="stylesheet" href="/css/materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/css/vlastni-styly.css">
  </head>
  <body>
      <%@include file="/WEB-INF/jspf/navbar.jspf" %>
    <main>

        <h4 class="text-bold center center-error">${status} ${error} - ${status == 404 ? "Tady nic není :(" : "Něco se tu pokazilo. Už na tom pracujeme"}</h4>

    </main>
      <%@include file="/WEB-INF/jspf/footer.jspf" %>
  </body>
</html>

