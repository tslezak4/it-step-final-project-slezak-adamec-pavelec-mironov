<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Detail objednavky</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/radioRedirect.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
       <c:set var="forAdmins" value="true" />
  <%@include file="/WEB-INF/jspf/navbar.jspf" %>
  <main>
    <div class="section">
      <div class="chip right">
        <img src="img/user.png" alt="">
        <sec:authentication property="principal.username" />
      </div>
    </div>

    <div class="container">
    <div class="section">
      <div class="row grey lighten-2">
        <div class="col s8 offset-s2">
          <ul class="collection with-header">
        <li class="collection-header ${orderItems.get(0).order.status.id == 1 ? '' : orderItems.get(0).order.status.id == 2 ? 'green' : 'red'} lighten-3">
          <div class="row">
            <div class="col s4">
              <h5>Objednávka #${orderItems.get(0).order.id}</h5>
              <span>Uživatel: ${orderItems.get(0).order.user.name}<br>Datum: ${orderItems.get(0).order.dateOrdered}</span>
            </div>

            <div class="col s2 offset-s4">
              <form action="#">
               <p>
                 <label class="black-text">
                   <input name="group1" value="/admin/orders/enable-order/${orderItems.get(0).order.id}/2" type="radio" <c:if test="${orderItems.get(0).order.status.id == 2}">checked</c:if>/>
                   <span>Potvrzeno</span>
                 </label>
               </p>
               <p>
                 <label class="black-text">
                   <input name="group1" value="/admin/orders/enable-order/${orderItems.get(0).order.id}/3" type="radio" <c:if test="${orderItems.get(0).order.status.id == 3}">checked</c:if>/>
                   <span>Stornováno</span>
                 </label>
               </p>
              </form>
            </div>
            <div class="col s1 delete-on-header">
              <a href="/admin/orders/delete-order/${orderItems.get(0).order.id}"><i class="material-icons">delete</i></a>
            </div>
          </div>

        <c:forEach items="${orderItems}" var="orderItem">
        </li>
        <li class="collection-item avatar ${orderItems.get(0).order.status.id == 1 ? '' : orderItems.get(0).order.status.id == 2 ? 'green' : 'red'} lighten-3">
          <img src="/image/${orderItem.goods.id}" alt="" class="circle col-mar-10top">
          <p class="title text-bold col-pad-20top">${orderItem.goods.name}</p>
          <p class="secondary-content black-text col-pad-15top">${orderItem.fixedPrice * orderItem.goodsQuantity} Kč</p>
          <%--inlinovy styl protoze si to nejak pamatuje ty styly a je uplne jedno, co s tim udelam (i kdyz ten styl smazu)--%>
          <p class="secondary-content black-text col-pad-15top col-mar-250right " style="margin-right: 180px">${orderItem.goodsQuantity}</p>
        </li>
        </c:forEach>
        <li class="collection-item ${orderItems.get(0).order.status.id == 1 ? '' : orderItems.get(0).order.status.id == 2 ? 'green' : 'red'} lighten-3">
            <c:set var="completePrice" value="0"/>
            <c:forEach items="${orderItems}" var="orderItem"><c:set var="completePrice" value="${completePrice + (orderItem.fixedPrice * orderItem.goodsQuantity)}"/></c:forEach>
          <span>Celkem:</span>
          <span class="secondary-content black-text text-bold">${completePrice} Kč</span>
        </li>
      </ul>
        </div>
      </div>
    </div>
  </div>
  </main>

  <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>