<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Objednávky</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/datepicker-options.js" charset="utf-8"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/radioRedirect.js" charset="utf-8"></script>
  <script src="/js/forModal.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
  <%@include file="/WEB-INF/jspf/modal-dialog.jspf" %>
  <c:set var="forAdmins" value="true" />
  <%@include file="/WEB-INF/jspf/navbar.jspf" %>
  <main>

    <div class="section">
      <div class="chip right">
        <img src="/img/user.png" alt="">
        <sec:authentication property="principal.username" />
      </div>
    </div>
<form:form method="post" action="/admin/orders" modelAttribute="orderFilterForm">
    <div class="container">
      <div class="section">
        <div class="row grey lighten-2 valign-wrapper">
          <div class="input-field col s2">
            <form:select path="sort">
              <form:option value="" disabled="true" selected="selected">Řadit podle</form:option>
              <form:option value="user">uživatelé</form:option>
              <form:option value="dateOrdered">datum</form:option>
            </form:select>
            <form:label path="sort">Řazení</form:label>
          </div>
          <div class="input-field col s2">
            <form:select path="direction">
              <form:option value="asc" selected="selected">Vzestupně</form:option>
              <form:option value="desc">Sestupně</form:option>
            </form:select>
          </div>
            <div class="input-field col s2">
              <form:input path="dateFrom" type="text" class="datepicker" placeholder="datum od"/>
              <form:label path="dateFrom">Datum</form:label>
            </div>
            <div class="input-field col s2" >
              <form:input path="dateTo" type="text" class="datepicker" placeholder="datum do"/>
            </div>
            <div class="input-field col s2">
              <form:select path="status">
                <form:option value="" disabled="true" selected="selected">status</form:option>
                <form:option value="1">čeká na potvrzení</form:option>
                <form:option value="2">potvrzeno</form:option>
                <form:option value="3">stornováno</form:option>
              </form:select>
              <form:label path="status">Status</form:label>
            </div>
            <div class="input-field col s2">
              <form:input path="search" type="text"/>
              <form:label path="search">uživatel/číslo</form:label>
            </div>
            <div class="col s1 valign">
              <button class="blue darken-2 btn waves-effect waves-light" type="submit" name="action">
                <i class="material-icons center">search</i>
              </button>
            </div>
      </div>
            <c:if test="${showResetButton}">
            <div class="col s1">
                <a class="btn blue darken-2 right" href="/admin/orders">RESET</a>
            </div>
            </c:if>
    </div>
  </div>
</form:form>      
      <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
      <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>
      
      <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>

  <div class="container">
    <div class="section">
      <div class="row grey lighten-2">
        <div class="col s8 offset-s2">
            <c:if test="${showResetButton && page.isEmpty()}">
                <c:set var="notFoundMessage" value="pro tento výběr nebylo nic nalezeno" />
                <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
            </c:if>
          <ul class="collection">
          
          <c:forEach items="${orders.getContent()}" var="order">
        <li class="collection-item ${order.status.id == 1 ? '' : order.status.id == 2 ? 'green' : 'red'} lighten-3">
          <div class="row" style="margin-bottom: 0px">
            <div class="col s4">
              <span class="title text-bold">Objednávka #${order.id}<br></span>
              <span>Uživatel: ${order.user.getName()}<br>Datum: ${order.dateOrdered}<br><a href="orders/detail/${order.id}" class="blue-text text-darken-2">Detail</a></span>
            </div>

            <div class="col s2 offset-s4">
              <form>
               <p>
                 <label class="black-text">
                   <input value="/admin/orders/enable-order/${order.id}/2" name="group${order.id}" type="radio" <c:if test="${order.status.id == 2}">checked</c:if>/>
                   <span>Potvrzeno</span>
                 </label>
               </p>
               <p>
                 <label class="black-text">
                   <input value="/admin/orders/enable-order/${order.id}/3" name="group${order.id}" type="radio" <c:if test="${order.status.id == 3}">checked</c:if>/>
                   <span>Stornováno</span>
                 </label>
               </p>
             </form>
            </div>
            <div class="col s1 delete-on-orders">
                <a href="#confirmation" class="modal-trigger"
                   data-modal-title="Odstranit objednávku"
                   data-modal-body="Opravdu chcete odstranit objednávku s id ${order.id}?"
                   data-modal-success-title="Odstranit"
                   data-modal-success-class="red"
                   data-modal-redirect="/admin/orders/delete-order/${order.id}">
                    <i class="material-icons">delete</i>
                </a>
            </div>
          </div>       
        </c:forEach>

      </ul>
        </div>
      </div>
    </div>
  </div>

    <c:if test="${goods.pageable.pageNumber - 1 > page.totalPages}">
        <c:set var="notFoundMessage" value="Strana ${goods.pageable.pageNumber} neexistuje" />
        <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
    </c:if>
  <c:if test="${orders.totalPages > 1}">
  <div class="section">
  <div class="row">
    <div class="col s8 offset-s2">
      <ul class="pagination center">
          <li class="${orders.isFirst() ? "disabled" : ""}"><a<c:if test="${!orders.isFirst()}"> href="?page=${orders.previousOrFirstPageable().pageNumber}&sort=${sort}&dateFrom=${dateFrom}&dateTo=${dateTo}&search=${search}&statusId=${statusId}"</c:if>><i class="material-icons">chevron_left</i></a></li>
        <c:forEach begin="1" end="${orders.totalPages}" varStatus="pageIter">
        <li class="waves-effect ${orders.pageable.pageNumber == pageIter.index - 1 ? "active blue darken-2" : ""}"><a href="?page=${pageIter.index - 1}&sort=${sort}&dateFrom=${dateFrom}&dateTo=${dateTo}&search=${search}&statusId=${statusId}">${pageIter.index}</a></li>
        </c:forEach>
        <li class="${orders.isLast() ? "disabled" : ""}"><a<c:if test="${!orders.isLast()}"> href="?page=${orders.nextOrLastPageable().pageNumber}&sort=${sort}&dateFrom=${dateFrom}&dateTo=${dateTo}&search=${search}&statusId=${statusId}"</c:if>><i class="material-icons">chevron_right</i></a></li>
      </ul>
    </div>
  </div>
</div>
</c:if>
  </main>

  <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>