<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Zboží</title>

  <script src="/js/multiple-select-fix.js" charset="utf-8"></script> <%--prozatimní fix--%>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/forModal.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
   <%@include file="/WEB-INF/jspf/modal-dialog.jspf" %>
   <c:set var="forAdmins" value="true" />
   <%@include file="/WEB-INF/jspf/navbar.jspf" %>

  <main>
    <div class="section">
      <div class="chip right">
        <img src="/img/user.png" alt="">
        <sec:authentication property="principal.username" />
      </div>
    </div>

    <div class="fixed-action-btn">
        <a href="goods/add-goods" class="btn-floating btn-large blue darken-4">
        <i class="large material-icons">add</i>
      </a>
    </div>

   <form:form method="post" action="/admin/goods" modelAttribute="goodsFilterForm">
    <div class="container">
      <div class="section">
        <div class="row grey lighten-2 valign-wrapper">
          <div class="input-field col s2">
            <form:select path="sort">
              <form:option value="" disabled="true" selected="selected">Řadit podle</form:option>
              <form:option value="price">cena</form:option>
              <form:option value="name">jméno</form:option>
              <form:option value="brand">značka</form:option>
            </form:select>
            <form:label path="sort">Řazení</form:label>
          </div>
          <div class="input-field col s2">
            <form:select path="direction">
              <form:option value="asc" selected="selected">Vzestupně</form:option>
              <form:option value="desc">Sestupně</form:option>
            </form:select>
          </div>
            <div class="input-field col s2 offset-s1">
              <form:select path="kategory" multiple="true">
                <form:option value="" disabled="true" selected="selected">kategorie</form:option>
                
                <c:forEach var="kategory" items="${kategories}">
                <form:option value="${kategory.id}">${kategory.name}</form:option>
                </c:forEach>
                
              </form:select>
                <form:label style="margin-bottom: 20px" path="kategory">Filtry</form:label>
            </div>
          
            <div class="input-field col s2">
              <form:select path="brand" multiple="multiple">
                <form:option value="" disabled="true" selected="selected">značky</form:option>
                
                <c:forEach items="${brands}" var="brand"> 
                
                <form:option value="${brand.id}">${brand.name}</form:option>
                                
                </c:forEach>
                
                
              </form:select>
            </div>
            <div class="input-field col s2">
              <form:input path="search" id="searchfield" type="text" class="validate"/>
              <form:label path="search" for="searchfield">název</form:label>
            </div>
            <div class="col s1 valign">
              <button class="blue darken-2 btn waves-effect waves-light" type="submit" name="action">
                <i class="material-icons center">search</i>
              </button>
                
            </div>
            <c:if test="${showResetButton}">
            <div class="col s1">
                <a class="btn blue darken-2" href="/admin/goods">RESET</a>
            </div>
            </c:if>
      </div>
    </div>
  </div>
  </form:form>
      
      <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
      <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>
      
      <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>

    <div class="container">
      <div class="section">
        <div class="row grey lighten-2">
          <div class="col s8 offset-s2">
              <c:if test="${showResetButton && page.isEmpty()}">
                <c:set var="notFoundMessage" value="pro tento výběr nebylo nic nalezeno" />
                <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
              </c:if>
              
            <ul class="collection">
                
             <c:forEach items="${page.getContent()}" var="goods">   
                
              <li class="collection-item avatar">
                <img src="/image/${goods.id}" alt="" class="circle">
                <span class="title text-bold">${goods.name}</span>
                <p>Cena: ${goods.price} Kč<br>
                   Značka: ${goods.brand.getName()}<br>
                                     
                   <c:forEach items="${goods.ketegories}" var="kategory">
                       ${kategory.name} 
                   </c:forEach>
                   
                </p>
                <a href="goods/edit-goods/${goods.id}" class="secondary-content blue-text text-darken-2 col-mar-30top col-mar-140right">Upravit</a>
                <a href="#confirmation" 
                   class="modal-trigger secondary-content blue-text text-darken-2 col-mar-30top col-mar-100right"
                   data-modal-title="Odstranit produkt"
                   data-modal-body="Opravdu chcete odstranit produkt ${goods.name}?"
                   data-modal-success-title="Odstranit"
                   data-modal-success-class="red"
                   data-modal-redirect="/admin/goods/delete-goods/${goods.id}">
                   <i class="material-icons">delete</i>
                </a>
                <a href="goods/enable-goods/${goods.id}" class="secondary-content ${goods.isEnabled == true ? 'green-text' : 'red-text'} col-mar-30top">${goods.isEnabled == true ? 'Aktivní' : 'Neaktivní'}</a>
              </li>
              
             </c:forEach> 

              
            </ul>
          </div>
        </div>
      </div>
    </div>
      
      <c:if test="${goods.pageable.pageNumber - 1 > page.totalPages}">
        <c:set var="notFoundMessage" value="Strana ${goods.pageable.pageNumber} neexistuje" />
        <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
    </c:if>
  <c:if test="${page.totalPages > 1}">    
  <div class="section">
    <div class="row">
      <div class="col s8 offset-s2">
        <ul class="pagination center">
            <li class="${page.isFirst() ? "disabled" : ""}"><a<c:if test="${!page.isFirst()}"> href="?page=${page.previousOrFirstPageable().pageNumber}&sort=${sort}&kategoryIds=${brandIds}&brandIds=${brandIds}&search=${search}"</c:if>><i class="material-icons">chevron_left</i></a></li>
          <c:forEach begin="1" end="${page.totalPages}" varStatus="pageIter">
          <li class="waves-effect ${page.pageable.pageNumber == pageIter.index - 1 ? "active blue darken-2" : ""}"><a href="?page=${pageIter.index - 1}&sort=${sort}&kategoryIds=${kategoryIds}&brandIds=${brandIds}&search=${search}">${pageIter.index}</a></li>
          </c:forEach>
          <li class="${page.isLast() ? "disabled" : ""}"><a<c:if test="${!page.isLast()}"> href="?page=${page.nextOrLastPageable().pageNumber}&sort=${sort}&kategoryIds=${kategoryIds}&brandIds=${brandIds}&search=${search}"</c:if>><i class="material-icons">chevron_right</i></a></li>
        </ul>
      </div>
    </div>
  </div>
  </c:if> 

</main>

  <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
