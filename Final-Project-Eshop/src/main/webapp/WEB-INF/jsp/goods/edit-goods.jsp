<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>
<c:set var="formTitle" value="Upravit zboží" />
<c:set var="formType" value="goodsForm"/>
<c:set var="formAction" value="${goodsId}"/>
<%@include file="/WEB-INF/jspf/goods-form.jspf" %>
