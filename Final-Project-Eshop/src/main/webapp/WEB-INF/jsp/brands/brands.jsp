<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/jspf/base.jspf" %>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Značky</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/myjs.js" charset="utf-8"></script>
  <script src="/js/forModal.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/css/materialize.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="/css/vlastni-styly.css">
</head>
<body>
   <%@include file="/WEB-INF/jspf/modal-dialog.jspf" %>
   <c:set var="forAdmins" value="true" />
   <%@include file="/WEB-INF/jspf/navbar.jspf" %>

  </nav>
  <main>

    <div class="section">
      <div class="chip right">
        <img src="/img/user.png" alt="">
        <sec:authentication property="principal.username" />
      </div>
    </div>

    <div class="fixed-action-btn">
        <a href="brands/add-brands" class="btn-floating btn-large blue darken-4">
        <i class="large material-icons">add</i>
      </a>
    </div>
      
      <c:if test="${successMessage != null}">
            <%@include file="/WEB-INF/jspf/success-message.jspf" %>
        </c:if>
      
      <c:if test="${errorMessage != null}">
            <%@include file="/WEB-INF/jspf/error-message.jspf" %>
        </c:if>
      
      <c:if test="${notFoundMessage != null}">
            <%@include file="/WEB-INF/jspf/not-found-message.jspf" %>
        </c:if>

    <div class="container">
      <div class="section grey lighten-2">
        <div class="row">
          <div class="col s8 offset-s2">
            <ul class="collection">
          <c:forEach items="${brands}" var="brand"> 

              <li class="collection-item avatar avatar-to-normal">

                      <h4>${brand.name}</h4>
                      <a href="#confirmation" 
                        class="modal-trigger secondary-content blue-text text-darken-2 col-mar-30top"
                        data-modal-title="Odstranit značku"
                        data-modal-body="Opravdu chcete odstranit značku ${brand.name}?"
                        data-modal-success-title="Odstranit"
                        data-modal-success-class="red"
                        data-modal-redirect="/admin/brands/delete-brand/${brand.id}">
                        <i class="material-icons">delete</i>
                      </a>
                      <a href="brands/edit-brand/${brand.id}" class="secondary-content blue-text text-darken-2 col-mar-30top col-mar-100right">Upravit</a>

              </li>            
              
          </c:forEach>
</ul>
          </div>
        </div>
      </div>
    </div>
  </main>

  <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
