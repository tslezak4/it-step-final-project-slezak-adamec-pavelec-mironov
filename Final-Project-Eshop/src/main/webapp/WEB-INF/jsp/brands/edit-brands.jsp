<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/base.jspf" %>
<c:set var="formTitle" value="Upravit značku" />
<c:set var="formAction" value="${brandId}"/>
<%@include file="/WEB-INF/jspf/brands-form.jspf" %>
