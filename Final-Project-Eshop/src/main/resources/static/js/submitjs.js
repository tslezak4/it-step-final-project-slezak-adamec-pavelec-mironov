$(function(){
    $('form').each(function(){
        
        let form = $(this);
        let loading = $('<img src="/img/loading.gif" class="submit-gif">').addClass('loading').hide();
        $('div[id="submitDiv"]', form).after(loading);
        
        form.submit(function(e){
            
            if(form.data('submitted') === true) {
                
                e.preventDefault();
            }
            
            else {
                
                form.data('submitted', true);
                $('button[type="submit"]', this).prop('disabled', true).addClass('disabled');
                loading.show();
            }
        });
    });
});

