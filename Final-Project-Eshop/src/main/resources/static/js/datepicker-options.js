$(function(){
    
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        firstDay: 1,
        i18n: {
            cancel: 'Zrušit',
            months: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
            monthsShort: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čer', 'Čen', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
            weekdays: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
            weekdaysShort: ['Ned', 'Pon', 'Úte', 'Stř', 'Čtv', 'Pát', 'Sob'],
            weekdaysAbbrev: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So']
        }
    });
});


