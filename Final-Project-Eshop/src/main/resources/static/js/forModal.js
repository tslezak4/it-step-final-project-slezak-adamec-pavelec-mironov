$(function(){
    
    for(let anchor of document.getElementsByClassName('modal-trigger'))
    {
        anchor.onclick= function(){window.lastAnchorClicked = this;};
    }

$('.modal').modal({

      onOpenStart: function() {
        
        let $button = window.lastAnchorClicked;
        let confirmButton = document.getElementById('confirm-button');
        
        confirmButton.onclick = function(){
            
            confirmButton.classList.add('disabled');
        };
        
        if($button.dataset.modalTitle)
        {
            document.getElementById('confirm-title').innerText = $button.dataset.modalTitle;
        }
        if($button.dataset.modalBody)
        {
            document.getElementById('confirm-body').innerText = $button.dataset.modalBody;
        }
        if($button.dataset.modalSuccessTitle)
        {
            confirmButton.innerText = $button.dataset.modalSuccessTitle;
        }
        if($button.dataset.modalSuccessClass)
        {
            confirmButton.classList.remove('blue');
            confirmButton.classList.add($button.dataset.modalSuccessClass);
        }
        if($button.dataset.modalRedirect)
        {
            confirmButton.href = $button.dataset.modalRedirect;
        }
      }
  });
});


