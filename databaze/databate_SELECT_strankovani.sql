-- Titulni stranka, zbozi. Bez razeni, bez filtrace.
-- Prvni 30 zbozi na stranke.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
LIMIT 30;

-- Titulni stranka, zbozi. Bez razeni, bez filtrace.
-- Dalsi 30 zbozi na stranke (stranka 2).
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
LIMIT 30, 30 ;

-- Titulni stranka, zbozi. Bez razeni, bez filtrace.
-- Treti 30 zbozi na stranke (stranka 3).
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
LIMIT 60, 30 ;

-- Titulni stranka, zbozi. Bez razeni, bez filtrace.
-- Sstranka 4 - mame jenom posledni 14 zbozi.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
LIMIT 90, 30 ;

-- Stejne muze byt spolu s razeni i filtrace. Napriklad:

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové.
-- Prvni 30 zbozi na stranke.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY price DESC
LIMIT 30;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové.
-- Druhi 30 zbozi na stranke - Stranka 2.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY price DESC
LIMIT 30, 30;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové.
-- Sstranka 3 - mame jenom posledni 6 zbozi.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY price DESC
LIMIT 60, 30;

