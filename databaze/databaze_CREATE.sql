
-- tohle se mozna potom bude predelavat
CREATE TABLE `images`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `content_type` VARCHAR(100) NOT NULL,
    `content` MEDIUMBLOB NOT NULL,
    
    PRIMARY KEY(`id`)
);

CREATE TABLE `status`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` NVARCHAR(30) NOT NULL,
    
    PRIMARY KEY(`id`),
    UNIQUE(`name`)
);

CREATE TABLE `kategories`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` NVARCHAR(30) NOT NULL,
    
    PRIMARY KEY(`id`),
    UNIQUE(`name`)
);

CREATE TABLE `brands`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(30) NOT NULL,
    
    PRIMARY KEY(`id`),
    UNIQUE(`name`)
);


CREATE TABLE `users`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` NVARCHAR(200) NOT NULL,
    `login` VARCHAR(30) NOT NULL UNIQUE KEY,
    `password` VARCHAR(100) NOT NULL,
    `is_enabled` INT(1) NOT NULL,
    `adress` VARCHAR(100) NOT NULL,
    
    PRIMARY KEY(`id`)
);

CREATE TABLE `roles`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `authority` VARCHAR(30) NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    
    PRIMARY KEY(`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE `orders`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `status_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    `date_ordered` DATE NOT NULL,
    
    PRIMARY KEY(`id`),
    FOREIGN KEY (`status_id`) REFERENCES `status`(`id`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE `goods`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `brand_id` INT UNSIGNED NOT NULL,
    `description` VARCHAR(500) NOT NULL,
    `price` INT UNSIGNED NOT NULL,
    `image_id` INT UNSIGNED NOT NULL,
    `is_enabled` INT(1) NOT NULL,
    
    PRIMARY KEY(`id`),
    FOREIGN KEY (`brand_id`) REFERENCES `brands`(`id`),
    FOREIGN KEY (`image_id`) REFERENCES `images`(`id`)
);

CREATE TABLE `order_item`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `goods_id` INT UNSIGNED NOT NULL,
    `goods_quantity` INT NOT NULL,
    `fixed_price` INT UNSIGNED NOT NULL,
    `order_id` INT UNSIGNED NOT NULL,
    
    PRIMARY KEY(`id`),
    FOREIGN KEY (`goods_id`) REFERENCES `goods`(`id`),
    FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`)
);

CREATE TABLE `goods_kategories`
(
	`goods_id` INT UNSIGNED NOT NULL,
	`kategory_id` INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (`goods_id`) REFERENCES `goods`(`id`),
    FOREIGN KEY (`kategory_id`) REFERENCES `kategories`(`id`)
);

CREATE TABLE `cart`
(
	`id` INT UNSIGNED AUTO_INCREMENT,
    `user_id` INT UNSIGNED NOT NULL,
    `goods_id` INT UNSIGNED NOT NULL,
    `goods_quantity` INT NOT NULL,
    
    PRIMARY KEY(`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users`(`id`),
    FOREIGN KEY (`goods_id`) REFERENCES `goods`(`id`)
);
