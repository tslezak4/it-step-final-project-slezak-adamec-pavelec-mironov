-- Titulni stranka, zbozi. Bez razeni, bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id;

-- Titulni stranka, zbozi. Bez razeni, filtrace - Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple');

-- Titulni stranka, zbozi. Bez razeni, filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia');

-- Titulni stranka, zbozi. Bez razeni, filtrace - Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei');

-- Titulni stranka, zbozi. Bez razeni, filtrace - Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple');

-- Titulni stranka, zbozi. Bez razeni, filtrace - Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia');

-- Titulni stranka, zbozi. Bez razeni, filtrace - Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové');

-- Titulni stranka, zbozi. Bez razeni, filtrace Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové');

-- Titulni stranka, zbozi. Bez razeni, filtrace Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové');

-- Titulni stranka, zbozi. Bez razeni, filtrace Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' );

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) ;

-- Titulni stranka, zbozi. Bez razeni, filtrace  Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory');

-- Titulni stranka, zbozi. Bez razeni, filtrace  Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové');

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace - Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace  Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price ;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace  Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace  Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace  Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY price ;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace  Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price ;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price  DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny -Sestupne. Filtrace:  Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price  DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle ceny - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY price DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli- Vzestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace - Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace  Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name ;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace  Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace  Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace  Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY goods.name ;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace  Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli -Sestupne. Filtrace:  Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Nazvu Modeli - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY goods.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace - Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace  Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace  Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace  Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace  Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace  Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Vzestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Bez filtrace.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Tlačítkové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Tlačítkové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Dotykové.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id 
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Apple')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace - Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Apple')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Huawei + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Huawei', 'Nokia')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek 
FROM goods, brands, images
WHERE goods.brand_id = brands.id AND goods.image_id = images.id AND brands.name IN ('Nokia', 'Apple')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
 ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
 ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu -Sestupne. Filtrace:  Dotykové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' )
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Huawei.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Huawei')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Nokia + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Nokia', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory') 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Tlačítkové + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Dotykové', 'Tlačítkové' ) 
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Tlačítkové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Tlačítkové', 'Pro seniory')
ORDER BY brands.name DESC;

-- Titulni stranka, zbozi. Razeni podle Brendu - Sestupne. Filtrace: Dotykové + Pro seniory + Huawei + Apple.
SELECT goods.name AS Model, brands.name AS Brend, description AS Popis, price AS Cena, is_enabled AS Sklad, images.name AS Obrazek
FROM goods
JOIN brands ON goods.brand_id = brands.id AND brands.name IN ('Huawei', 'Apple')
JOIN images ON goods.image_id = images.id
JOIN goods_kategories ON goods_kategories.goods_id = goods.id
JOIN kategories ON kategories.id = goods_kategories.kategory_id AND kategories.name IN ('Pro seniory', 'Dotykové')
ORDER BY brands.name DESC;