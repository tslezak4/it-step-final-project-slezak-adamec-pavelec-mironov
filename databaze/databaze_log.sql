DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log`
(
	id BIGINT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    entity VARCHAR(100) NOT NULL,
    `action` VARCHAR(100) NOT NULL,
    `message` VARCHAR(500) NOT NULL,
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    INDEX(entity),
    INDEX(`action`),
    INDEX(date_created)
);

DELIMITER //

DROP PROCEDURE IF EXISTS log_event //
CREATE PROCEDURE log_event (
	IN entity VARCHAR(100),
    IN `action` VARCHAR(100),
    IN `message` VARCHAR(500)
)
BEGIN
	INSERT INTO `log` (entity, `action`, `message`)
    VALUES (entity, `action`, `message`);
END //

DROP TRIGGER IF EXISTS log_brand_insert //
CREATE TRIGGER log_brand_insert
AFTER INSERT ON brands
FOR EACH ROW
BEGIN
	CALL log_event('brand', 'insert', CONCAT('Nová značka s id ', NEW.id, ' byla vložena'));
END //

DROP TRIGGER IF EXISTS log_brand_update //
CREATE TRIGGER log_brand_update
AFTER UPDATE ON brands
FOR EACH ROW
BEGIN
	CALL log_event('brand', 'update', CONCAT('Značka s id ', OLD.id, ' byla upravena'));
END //

DROP TRIGGER IF EXISTS log_brand_delete //
CREATE TRIGGER log_brand_delete
AFTER DELETE ON brands
FOR EACH ROW
BEGIN
	CALL log_event('brand', 'delete', CONCAT('Značka s id ', OLD.id, ' byla smazána'));
END //

DROP TRIGGER IF EXISTS log_user_insert //
CREATE TRIGGER log_user_insert
AFTER INSERT ON users
FOR EACH ROW
BEGIN
	CALL log_event('user', 'insert', CONCAT('Nový uživatel s id ', NEW.id, ' byl vložen'));
END //

DROP TRIGGER IF EXISTS log_user_update //
CREATE TRIGGER log_user_update
AFTER UPDATE ON users
FOR EACH ROW
BEGIN
	CALL log_event('user', 'update', CONCAT('Uživatel s id ', OLD.id, ' byl upraven'));
END //

DROP TRIGGER IF EXISTS log_user_delete //
CREATE TRIGGER log_user_delete
AFTER DELETE ON users
FOR EACH ROW
BEGIN
	CALL log_event('user', 'delete', CONCAT('Uživatel s id ', OLD.id, ' byl smazán'));
END //

DROP TRIGGER IF EXISTS log_order_insert //
CREATE TRIGGER log_order_insert
AFTER INSERT ON orders
FOR EACH ROW
BEGIN
	CALL log_event('order', 'insert', CONCAT('Nová objednávka s id ', NEW.id, ' byla vytvořena'));
END //

DROP TRIGGER IF EXISTS log_order_update //
CREATE TRIGGER log_order_update
AFTER UPDATE ON orders
FOR EACH ROW
BEGIN
	CALL log_event('order', 'update', CONCAT('Status objednávky s id ', OLD.id, ' byl změněn'));
END //

DROP TRIGGER IF EXISTS log_order_delete //
CREATE TRIGGER log_order_delete
AFTER DELETE ON orders
FOR EACH ROW
BEGIN
	CALL log_event('order', 'delete', CONCAT('Objednávka s id ', OLD.id, ' byla smazána'));
END //

DROP TRIGGER IF EXISTS log_goods_insert //
CREATE TRIGGER log_goods_insert
AFTER INSERT ON goods
FOR EACH ROW
BEGIN
	CALL log_event('goods', 'insert', CONCAT('Nové zboží s id ', NEW.id, ' bylo vloženo'));
END //

DROP TRIGGER IF EXISTS log_goods_update //
CREATE TRIGGER log_goods_update
AFTER UPDATE ON goods
FOR EACH ROW
BEGIN
	CALL log_event('goods', 'update', CONCAT('Zboží s id ', OLD.id, ' bylo upraveno'));
END //

DROP TRIGGER IF EXISTS log_goods_delete //
CREATE TRIGGER log_goods_delete
AFTER DELETE ON goods
FOR EACH ROW
BEGIN
	CALL log_event('goods', 'delete', CONCAT('Zboží s id ', OLD.id, ' bylo smazáno'));
END //

DELIMITER ;