SELECT * FROM goods;
SELECT * FROM brands;

-- Stranka pro spravce, objednavky. Bez razeni, bez filtrace.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id;

-- Stranka pro spravce, objednavky. Bez razeni. Filtrace: Potvrzena.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id;

-- Stranka pro spravce, objednavky. Bez razeni. Filtrace: Stornovana.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id;

-- Stranka pro spravce, objednavky. Bez razeni. Filtrace: od 01.06.2020 az 30.06.2020.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30';

-- Stranka pro spravce, objednavky. Bez razeni. Filtrace: od 01.06.2020 az 30.06.2020 ; Potvrzena. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30';

-- Stranka pro spravce, objednavky. Bez razeni. Filtrace: od 01.06.2020 az 30.06.2020 ; Stornovana. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30';

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne, bez filtrace.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne. Filtrace: Potvrzena.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne. Filtrace: Stornovana.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Potvrzena. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Stornovana. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne, bez filtrace.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
ORDER BY users.name DESC;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne. Filtrace: Potvrzena.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
ORDER BY users.name DESC;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne. Filtrace: Stornovana.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
ORDER BY users.name DESC;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name  DESC;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Potvrzena. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name DESC;

-- Stranka pro spravce, objednavky. Razeni podle Zakazniku - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Stornovana. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY users.name DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne, bez filtrace.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne. Filtrace: Potvrzena.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne. Filtrace: Stornovana.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Potvrzena. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Vzestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Stornovana. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne, bez filtrace.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne. Filtrace: Potvrzena.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne. Filtrace: Stornovana.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
ORDER BY date_ordered DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020.
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id 
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Potvrzena. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Potvrzena')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered DESC;

-- Stranka pro spravce, objednavky. Razeni podle Daty - Sestupne. Filtrace: od 01.06.2020 az 30.06.2020 ; Stornovana. 
SELECT orders.id AS Cislo, users.name AS Zakaznik, date_ordered AS Datum, status.name AS Status
FROM orders
JOIN status ON orders.status_id = status.id AND status.name IN ('Stornovana')
JOIN users ON orders.user_id = users.id
WHERE date_ordered BETWEEN '2020-06-01' AND '2020-06-30'
ORDER BY date_ordered DESC;

-- Dotaz na polozky podle cislu objednavky. Napriklad objednavka # 5.
SELECT images.name AS Obrazek, goods.name AS Model, order_item.goods_quantity AS Mnozstvi, order_item.fixed_price AS Cena
FROM orders 
JOIN order_item ON orders.id = order_item.order_id 
JOIN goods ON goods.id = order_item.goods_id
JOIN images ON goods.image_id = images.id
WHERE orders.id = 5;